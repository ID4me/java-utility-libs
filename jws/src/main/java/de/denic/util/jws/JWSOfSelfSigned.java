/*^
  ===========================================================================
  ID4me Utility Libs
  ===========================================================================
  Copyright (C) 2020 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.util.jws;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.nimbusds.jose.*;
import com.nimbusds.jose.jwk.JWK;
import com.nimbusds.jose.jwk.JWKSet;

import javax.annotation.Nonnull;
import javax.annotation.concurrent.Immutable;
import java.text.ParseException;
import java.util.Optional;
import java.util.function.Function;

import static java.util.Objects.requireNonNull;

public interface JWSOfSelfSigned<PAYLOAD_TYPE extends JWKSetSupplier> extends JWSOf<PAYLOAD_TYPE> {

  default boolean verifiableSelfSigned() {
    return getOptVerifyingSelfSigningKeyID().isPresent();
  }

  @Nonnull
  default Optional<String> getOptVerifyingSelfSigningKeyID() {
    return getOptVerifyingKeyIDFrom(getPayload().getJWKSet());
  }

  @Nonnull
  default String getVerifyingSelfSigningKeyID() throws JWSVerificationException {
    return getVerifyingKeyIDFrom(getPayload().getJWKSet());
  }

  @Nonnull
  static <PTYPE extends JWKSetSupplier> JWSOfSelfSigned<PTYPE> from(@Nonnull final String jwsData,
                                                                    @Nonnull final Class<? extends PTYPE> payloadType) throws JWSParsingException {
    final JWSObject jwsObject;
    try {
      jwsObject = JWSObject.parse(requireNonNull(jwsData, "Missing JWS data"));
    } catch (final ParseException e) {
      throw new JWSParsingException("JSON-parsing failed with '" + jwsData + "'", e);
    }

    return from(jwsObject, payloadType);
  }

  @Nonnull
  static <PTYPE extends JWKSetSupplier> JWSOfSelfSigned<PTYPE> from(@Nonnull final JWSObject jwsObject,
                                                                    @Nonnull final Class<? extends PTYPE> payloadType) {
    final PTYPE payloadData = requireNonNull(jwsObject, "Missing JWS instance").getPayload().toType(payload -> {
      try {
        return OBJECT_MAPPER.readerFor(requireNonNull(payloadType, "Missing type of payload")).readValue(payload.toString());
      } catch (final JsonProcessingException e) {
        throw new JWSParsingException("Unmarshalling JWS payload to " + payloadType + " failed", e);
      }
    });

    return new Impl<>(payloadData, jwsObject);
  }

  @Nonnull
  static <PTYPE extends JWKSetSupplier> JWSOfSelfSigned<PTYPE> signedWithIts1stJWK(@Nonnull final PTYPE payload) {
    return signed(payload, jwkSet -> jwkSet.getKeys().get(0));
  }

  @Nonnull
  static <PTYPE extends JWKSetSupplier> JWSOfSelfSigned<PTYPE> signed(@Nonnull final PTYPE payload,
                                                                      @Nonnull final Function<JWKSet, JWK> jwkSelector) {
    final JWK selectedJWK = requireNonNull(jwkSelector, "Missing JWK selector")
            .apply(requireNonNull(payload, "Missing payload").getJWKSet());
    final JWSSigner signer = JWSOf.createMatchingSignerOf(selectedJWK);
    final String serializedPayload;
    try {
      serializedPayload = OBJECT_MAPPER.writerFor(payload.getClass())
              .writeValueAsString(payload);
    } catch (final JsonProcessingException e) {
      throw new RuntimeException("Serializing payload '" + payload + "' failed", e);
    }

    final JWSObject jwsObject = new JWSObject(
            new JWSHeader.Builder(JWSAlgorithm.ES256).keyID(selectedJWK.getKeyID()).build(),
            new Payload(serializedPayload));
    try {
      jwsObject.sign(signer);
    } catch (final JOSEException e) {
      throw new RuntimeException("Singing payload '" + payload + "' failed", e);
    }

    return new JWSOfSelfSigned.Impl<>(payload, jwsObject);
  }

  @Immutable
  class Impl<PAYLOAD_TYPE extends JWKSetSupplier> extends JWSOf.Impl<PAYLOAD_TYPE> implements JWSOfSelfSigned<PAYLOAD_TYPE> {

    protected Impl(@Nonnull final PAYLOAD_TYPE payload,
                   @Nonnull final JWSObject jws) {
      super(payload, jws);
    }

  }

}
