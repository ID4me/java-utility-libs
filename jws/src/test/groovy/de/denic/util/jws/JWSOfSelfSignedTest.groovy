/*^
  ===========================================================================
  ID4me Utility Libs
  ===========================================================================
  Copyright (C) 2020 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.util.jws

import com.nimbusds.jose.JWSAlgorithm
import com.nimbusds.jose.JWSHeader
import com.nimbusds.jose.JWSObject
import com.nimbusds.jose.Payload
import spock.lang.Specification

import static JWSOfSelfSigned.from
import static de.denic.util.jws.JWSOfTest.*

class JWSOfSelfSignedTest extends Specification {

  def 'SUCCESSFUL verfication of correctly self-signed JWS'() {
    given:
    JWSObject jws = new JWSObject(
            new JWSHeader.Builder(JWSAlgorithm.ES256).keyID(JWK.getKeyID()).build(),
            new Payload(TEST_PAYLOAD))
    jws.sign(SIGNER)
    JWSOfSelfSigned<GineaPigJWKSetSupplier> CUT = from(jws.serialize(), GineaPigJWKSetSupplier.class)

    expect:
    CUT.verifiableWith(PUBLIC_JWK_SET)
    CUT.verifiableSelfSigned()
    CUT.getVerifyingSelfSigningKeyID() == KEY_ID
  }

  def 'Verfication of payload-modified self-signed JWS FAILED'() {
    given:
    JWSObject correctJWS = new JWSObject(
            new JWSHeader.Builder(JWSAlgorithm.ES256).keyID(JWK.getKeyID()).build(),
            new Payload(TEST_PAYLOAD))
    correctJWS.sign(SIGNER)
    def modifiedPayload = TEST_PAYLOAD.replace('subject', 'tcejbus')
    JWSObject payloadModifiedJWS = new JWSObject(correctJWS.header.toBase64URL(),
            new Payload(modifiedPayload),
            correctJWS.signature)
    JWSOfSelfSigned<GineaPigJWKSetSupplier> CUT = from(payloadModifiedJWS.serialize(), GineaPigJWKSetSupplier.class)

    expect:
    !CUT.verifiableWith(PUBLIC_JWK_SET)
    !CUT.verifiableSelfSigned()
    CUT.getOptVerifyingSelfSigningKeyID().isEmpty()
  }

}
