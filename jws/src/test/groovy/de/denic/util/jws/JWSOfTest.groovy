/*^
  ===========================================================================
  ID4me Utility Libs
  ===========================================================================
  Copyright (C) 2020 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.util.jws

import com.fasterxml.jackson.databind.JsonNode
import com.nimbusds.jose.*
import com.nimbusds.jose.crypto.ECDSASigner
import com.nimbusds.jose.jwk.Curve
import com.nimbusds.jose.jwk.ECKey
import com.nimbusds.jose.jwk.JWKSet
import com.nimbusds.jose.jwk.gen.ECKeyGenerator
import spock.lang.Specification

import static JWSOf.from

class JWSOfTest extends Specification {

  static final String KEY_ID = 'test-key'
  static final ECKey JWK = new ECKeyGenerator(Curve.P_256)
          .keyID(KEY_ID)
          .generate()
  static final ECKey PUBLIC_JWK = JWK.toPublicJWK()
  static final JWKSet PUBLIC_JWK_SET = new JWKSet(PUBLIC_JWK)
  static final JWSSigner SIGNER = new ECDSASigner(JWK)
  static final String TEST_PAYLOAD = """
          {
            "iss": "https://issuer.uri",
            "sub": "https://subject.uri",
            "iat": 1577836800,
            "exp": 4102444800,
            "jwks": {
              "keys": [
                ${PUBLIC_JWK.toJSONString()}
              ]
            }
          }"""

  def 'SUCCESSFUL verfication of correctly signed JWS'() {
    given:
    JWSObject jws = new JWSObject(
            new JWSHeader.Builder(JWSAlgorithm.ES256).keyID(JWK.getKeyID()).build(),
            new Payload(TEST_PAYLOAD))
    jws.sign(SIGNER)
    JWSOf<JsonNode> CUT = from(jws.serialize(), JsonNode)

    expect:
    CUT.verifiableWith(PUBLIC_JWK_SET)
    CUT.getVerifyingKeyIDFrom(PUBLIC_JWK_SET) == KEY_ID
  }

  def 'Verfication of payload-modified JWS FAILED'() {
    given:
    JWSObject correctJWS = new JWSObject(
            new JWSHeader.Builder(JWSAlgorithm.ES256).keyID(JWK.getKeyID()).build(),
            new Payload(TEST_PAYLOAD))
    correctJWS.sign(SIGNER)
    def modifiedPayload = TEST_PAYLOAD.replace('subject', 'tcejbus')
    JWSObject payloadModifiedJWS = new JWSObject(correctJWS.header.toBase64URL(),
            new Payload(modifiedPayload),
            correctJWS.signature)
    JWSOf<JsonNode> CUT = from(payloadModifiedJWS.serialize(), JsonNode)

    expect:
    !CUT.verifiableWith(PUBLIC_JWK_SET)
    CUT.getOptVerifyingKeyIDFrom(PUBLIC_JWK_SET).isEmpty()
  }

  def 'Verification of signed JWS from business object'() {
    given:
    def uriToSign = URI.create('http://uri.to/sign')
    JWSOf<URI> CUT = JWSOf.signed(uriToSign, JWK)

    expect:
    println "JWS-signed URI: ${CUT}"
    CUT.verifiableWith(PUBLIC_JWK_SET)
    CUT.getPayload() == uriToSign
  }

}
