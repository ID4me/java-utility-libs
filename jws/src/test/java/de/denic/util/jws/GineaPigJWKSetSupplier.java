/*^
  ===========================================================================
  ID4me Utility Libs
  ===========================================================================
  Copyright (C) 2020 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.util.jws;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.JsonNode;
import com.nimbusds.jose.jwk.JWKSet;

import javax.annotation.Nonnull;
import java.text.ParseException;

@JsonIgnoreProperties(ignoreUnknown = true)
public class GineaPigJWKSetSupplier implements JWKSetSupplier {

  private final JWKSet jwkSet;

  GineaPigJWKSetSupplier(@Nonnull final JWKSet jwkSet) {
    this.jwkSet = jwkSet;
  }

  GineaPigJWKSetSupplier(@Nonnull @JsonProperty("jwks") final JsonNode jwksJsonValue) {
    this(parseToJWKSet(jwksJsonValue));
  }

  @Nonnull
  private static JWKSet parseToJWKSet(@Nonnull final JsonNode jsonNode) {
    try {
      return JWKSet.parse(jsonNode.toString());
    } catch (final ParseException e) {
      throw new IllegalArgumentException("Parsing value of 'jwks' element failed", e);
    }
  }

  @Nonnull
  @Override
  public JWKSet getJWKSet() {
    return jwkSet;
  }

}
