/*^
  ===========================================================================
  ID4me Utility Libs
  ===========================================================================
  Copyright (C) 2020 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.util.http.impl;

import de.denic.util.http.URIBuilder;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.NotThreadSafe;
import java.net.URI;
import java.net.URISyntaxException;

import static java.util.Objects.requireNonNull;

@NotThreadSafe
public final class URIBuilderJDKAdapter implements URIBuilder {

  private final String scheme;
  private final String userInfo;
  private final String host;
  private final int port;
  private String path;
  private String query;
  private final String fragment;

  public URIBuilderJDKAdapter(@Nonnull final URI initialURI) {
    this.scheme = requireNonNull(initialURI, "Missing initial URI").getScheme();
    this.userInfo = initialURI.getUserInfo();
    this.host = initialURI.getHost();
    this.port = initialURI.getPort();
    this.path = initialURI.getPath();
    this.query = initialURI.getQuery();
    this.fragment = initialURI.getFragment();
  }

  @Nonnull
  @Override
  public URIBuilder replacePath(@Nullable final String newPath) {
    this.path = newPath;
    return this;
  }

  @Nonnull
  @Override
  public URIBuilder addPath(@Nonnull final String path) {
    this.path += (path.startsWith("/") ? path : "/" + path);
    return this;
  }

  @Nonnull
  @Override
  public URIBuilder queryParam(@Nonnull final String key, @Nonnull final Object param) {
    this.query += (this.query.isEmpty() ? "" : ",")
            + requireNonNull(key, "Missing query parameter key") + "="
            + requireNonNull(param, "Missing query parameter value").toString();
    return this;
  }

  @Nonnull
  @Override
  public URI build() {
    try {
      return new URI(scheme, userInfo, host, port, path, query, fragment);
    } catch (final URISyntaxException e) {
      throw new RuntimeException("Internal error", e);
    }
  }

}
