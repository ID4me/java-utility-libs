/*^
  ===========================================================================
  ID4me Utility Libs
  ===========================================================================
  Copyright (C) 2020 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.util.http.impl;

import de.denic.util.http.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;
import javax.annotation.concurrent.NotThreadSafe;
import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.Map;

import static de.denic.util.http.HTTPClient.replaceVerticalWhiteSpaces;
import static de.denic.util.http.HTTPClient.shortenedBody;
import static java.net.http.HttpRequest.BodyPublishers.ofString;
import static java.net.http.HttpResponse.BodyHandlers.ofString;
import static java.util.Objects.requireNonNull;

@Immutable
public final class HTTPClientJDKAdapter implements HTTPClient {

  private static final Logger LOG = LoggerFactory.getLogger(HTTPClientJDKAdapter.class);

  private final HttpClient httpClient;

  public HTTPClientJDKAdapter(@Nonnull final HttpClient httpClient) {
    this.httpClient = requireNonNull(httpClient, "Missing HTTP client");
  }

  @Nonnull
  @Override
  public String responseBodyQuerying(@Nonnull final URI wellKnownURI,
                                     @Nullable final Map<String, Object> optQueryParameters) throws HTTPException {
    final URI targetURI;
    if (optQueryParameters == null || optQueryParameters.isEmpty()) {
      targetURI = wellKnownURI;
    } else {
      final URIBuilder targetURIBuilder = uriBuilderOf(wellKnownURI);
      optQueryParameters.forEach(targetURIBuilder::queryParam);
      targetURI = targetURIBuilder.build();
    }
    final HttpRequest getRequest = HttpRequest.newBuilder(targetURI)
            .GET()
            .build();
    return execute("Queried", getRequest, "Querying");
  }

  @Nonnull
  @Override
  public RequestParamSetter sendingTo(@Nonnull final URI targetURI) {
    return new RequestBuilderImpl(targetURI);
  }

  @Nonnull
  private String execute(@Nonnull final String successLogMessagePrefix,
                         @Nonnull final HttpRequest request,
                         @Nonnull final String failureLogMessagePrefix) {
    try {
      final HttpResponse<String> response = httpClient.send(request, ofString());
      final String responseBody = response.body();
      final int httpStatusCode = response.statusCode();
      if (httpStatusCode < 400) {
        if (LOG.isDebugEnabled()) {
          LOG.debug("{} '{}': '{}'", successLogMessagePrefix, request.uri(), replaceVerticalWhiteSpaces(responseBody));
        }
        return responseBody;
      }

      if (LOG.isDebugEnabled()) {
        LOG.debug("{} '{}' failed. Response code '{}', body: '{}'",
                failureLogMessagePrefix, request.uri(), httpStatusCode, shortenedBody(responseBody));
      }
      throw new HTTPResponseException(httpStatusCode, null, responseBody);
    } catch (final IOException e) {
      LOG.info("{} '{}' failed: {} ({})", failureLogMessagePrefix, request.uri(), e.getMessage(), e.getClass().getName());
      throw new HTTPException(e.getMessage());
    } catch (final InterruptedException e) {
      Thread.currentThread().interrupt();
      LOG.info("{} '{}' interrupted.", failureLogMessagePrefix, request.uri(), e);
      throw new RuntimeException("Interrupted!", e);
    } catch (final RuntimeException e) {
      LOG.warn("{} '{}' failed.", failureLogMessagePrefix, request.uri(), e);
      throw e;
    }
  }

  @Nonnull
  @Override
  public URIBuilder uriBuilderOf(@Nonnull final URI initialURI) {
    return new URIBuilderJDKAdapter(initialURI);
  }

  @NotThreadSafe
  private class RequestBuilderImpl implements RequestParamSetter, RequestParamSetter.RequestSender {

    private final URI targetURI;
    private String mediaType;

    RequestBuilderImpl(final URI targetURI) {
      this.targetURI = requireNonNull(targetURI, "Missing target URI");
    }

    @Nonnull
    @Override
    public RequestParamSetter.RequestSender asMediaType(@Nonnull final String mediaType) {
      this.mediaType = requireNonNull(mediaType, "Missing media type");
      return this;
    }

    @Nonnull
    @Override
    public String responseBodyAfterPOSTing(@Nonnull final String requestBody) throws HTTPException {
      final HttpRequest postRequest = HttpRequest.newBuilder(targetURI)
              .setHeader("Content-Type", mediaType)
              .POST(ofString(requireNonNull(requestBody, "Missing request body content")))
              .build();
      return execute("POSTed to", postRequest, "POSTing to");
    }

  }

}
