/*^
  ===========================================================================
  ID4me Utility Libs
  ===========================================================================
  Copyright (C) 2020 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.util.flyweight

import com.fasterxml.jackson.databind.ObjectMapper
import spock.lang.Specification

import javax.annotation.Nonnull
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.ConcurrentMap

class FlyweigthTest extends Specification {

  static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

  def 'Predefined and newly created instances are not only equal BUT ALSO the same'() {
    given:
    def locallyCreatedJIMMY = OpenGuineaPig.of('JIMMY')

    expect:
    OpenGuineaPig.JIMMY == locallyCreatedJIMMY
    locallyCreatedJIMMY == OpenGuineaPig.JIMMY
    OpenGuineaPig.JIMMY.is(locallyCreatedJIMMY)
    locallyCreatedJIMMY.is(OpenGuineaPig.JIMMY)
  }

  def 'Equal-valued newly created instances are not only equal BUT ALSO the same'() {
    given:
    def firstJoe = OpenGuineaPig.of('JOE')
    def anotherJoe = OpenGuineaPig.of(firstJoe.value())

    expect:
    anotherJoe == firstJoe
    firstJoe == anotherJoe
    anotherJoe.is(firstJoe)
    firstJoe.is(anotherJoe)
  }

  def 'Instances are not only considered equal BUT ALSO the same case-INSENSITIVELY'() {
    given:
    def upperCasedJoe = OpenGuineaPig.of('JOE')
    def lowerCasedJoe = OpenGuineaPig.of(upperCasedJoe.lower())

    expect:
    lowerCasedJoe == upperCasedJoe
    upperCasedJoe == lowerCasedJoe
    lowerCasedJoe.is(upperCasedJoe)
    upperCasedJoe.is(lowerCasedJoe)
  }

  def 'STRICTLY predefined values are valid, equal, and the same'() {
    given:
    def locallyCreatedStrictJimmy = StrictGuineaPig.of('STRICT_JIMMY')

    expect:
    StrictGuineaPig.STRICT_JIMMY == locallyCreatedStrictJimmy
    locallyCreatedStrictJimmy == StrictGuineaPig.STRICT_JIMMY
    StrictGuineaPig.STRICT_JIMMY.is(locallyCreatedStrictJimmy)
    locallyCreatedStrictJimmy.is(StrictGuineaPig.STRICT_JIMMY)
  }

  def 'Custom values with STRICT Flyweights are invalid'() {
    when:
    StrictGuineaPig.of('OTHER_JIMMY')

    then:
    thrown(IllegalArgumentException)
  }

  def 'JSON-serialisation of STRICT and OPEN Flyweights'() {
    given:
    def openGuineaPig = OpenGuineaPig.JIMMY
    def strictGuineaPig = StrictGuineaPig.STRICT_JIMMY

    expect:
    OBJECT_MAPPER.writerFor(OpenGuineaPig).writeValueAsString(openGuineaPig) == '"jimmy"'
    OBJECT_MAPPER.writerFor(StrictGuineaPig).writeValueAsString(strictGuineaPig) == '"strict_jimmy"'
  }

}

final class OpenGuineaPig extends OpenFlyweigth<OpenGuineaPig> {

  private static final ConcurrentMap<String, OpenGuineaPig> FLYWEIGHTS = new ConcurrentHashMap<>()

  static final OpenGuineaPig JIMMY = of('JIMMY')

  private OpenGuineaPig(@Nonnull final String value) {
    super(value)
  }

  /**
   * Using Groovy's method pointer operator <code>.&</code>: https://docs.groovy-lang.org/latest/html/documentation/#method-pointer-operator
   */
  @Nonnull
  static OpenGuineaPig of(@Nonnull final String value) {
    OpenFlyweigth.of(value, FLYWEIGHTS, OpenGuineaPig.metaClass.&invokeConstructor)
  }

}

final class StrictGuineaPig extends StrictFlyweigth<OpenGuineaPig> {

  private static final ConcurrentMap<String, StrictGuineaPig> FLYWEIGHTS = new ConcurrentHashMap<>()

  /**
   * Using Groovy's method pointer operator <code>.&</code>: https://docs.groovy-lang.org/latest/html/documentation/#method-pointer-operator
   */
  static final StrictGuineaPig STRICT_JIMMY = preregisterAsValid('STRICT_JIMMY', FLYWEIGHTS, StrictGuineaPig.metaClass.&invokeConstructor)

  private StrictGuineaPig(@Nonnull final String value) {
    super(value)
  }

  @Nonnull
  static StrictGuineaPig of(@Nonnull final String value) {
    StrictFlyweigth.of(value, FLYWEIGHTS)
  }

}
