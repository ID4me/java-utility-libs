/*^
  ===========================================================================
  ID4me Utility Libs
  ===========================================================================
  Copyright (C) 2020 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.util.flyweight;

import com.fasterxml.jackson.annotation.JsonValue;

import javax.annotation.Nonnull;
import javax.annotation.concurrent.Immutable;
import java.util.concurrent.ConcurrentMap;
import java.util.function.Function;

import static java.util.Locale.ENGLISH;
import static java.util.Objects.requireNonNull;

/**
 * Base type to implement <a href="https://en.wikipedia.org/wiki/Flyweight_pattern">Flyweights</a>,
 * one of the <a href="https://en.wikipedia.org/wiki/Design_Patterns">Gang Of Four" design patterns</a>.
 * <br/>
 * We <strong>strongly encourage</strong> you to also have a look at the information given here {@link StrictFlyweigth#of(String, ConcurrentMap)}, and here {@link OpenFlyweigth#of(String, ConcurrentMap, Function)}.
 * <br/>
 * Predefined natural order is alphabetic (but might be overwritten in sub-classes).
 */
@Immutable
abstract class FlyweigthBase<TYPE extends FlyweigthBase<TYPE>> implements Comparable<TYPE> {

  private final String uppercase, lowercase;
  private final int hashCode;

  protected FlyweigthBase(@Nonnull final String value) {
    final String trimmedValue = requireNotEmptyTrimmed(value);
    this.uppercase = trimmedValue.toUpperCase(ENGLISH);
    this.lowercase = trimmedValue.toLowerCase(ENGLISH);
    this.hashCode = preCalculateHashCode(value);
  }

  @Nonnull
  public static String requireNotEmptyTrimmed(@Nonnull final String value) {
    final String trimmedValue = requireNonNull(value, "Missing value").trim();
    if (trimmedValue.isEmpty()) {
      throw new IllegalArgumentException("Missing or empty value");
    }

    return trimmedValue;
  }

  private static int preCalculateHashCode(@Nonnull final String value) {
    return 31 * value.hashCode();
  }

  /**
   * @return JSON-value representation, therefore <code>lowercase</code>.
   */
  @JsonValue
  @Nonnull
  public final String value() {
    return lowercase;
  }

  @Nonnull
  public final String upper() {
    return value();
  }

  @Nonnull
  public final String lower() {
    return lowercase;
  }

  @Override
  public int compareTo(final TYPE o) {
    return uppercase.compareTo(o.value());
  }

  @Override
  public final boolean equals(final Object o) {
    return this == o;
  }

  @Override
  public final int hashCode() {
    return hashCode;
  }

  @Override
  public String toString() {
    return uppercase;
  }

}
