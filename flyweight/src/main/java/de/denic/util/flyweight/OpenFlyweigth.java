/*^
  ===========================================================================
  ID4me Utility Libs
  ===========================================================================
  Copyright (C) 2020 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.util.flyweight;

import javax.annotation.Nonnull;
import javax.annotation.concurrent.Immutable;
import java.util.concurrent.ConcurrentMap;
import java.util.function.Function;

import static java.util.Locale.ENGLISH;

/**
 * Base type of <strong>open/non-strict</strong> {@link FlyweigthBase}: There are common predefined instances, but others/custom ones are allowed.
 *
 * @see FlyweigthBase
 */
@Immutable
public abstract class OpenFlyweigth<TYPE extends OpenFlyweigth<TYPE>> extends FlyweigthBase<TYPE> {

  protected OpenFlyweigth(@Nonnull final String value) {
    super(value);
  }

  /**
   * Beware of the fact that instances are referenced internally by their <strong>uppercased</strong> values! This happens for two reasons:
   * <ol>
   *   <li>Using elements of these types as names of database table's or node's fields/properties will be case-insensitive.</li>
   *   <li>It makes it easier to distinguish these Flyweight types from other ones, e.g. in {@link Object#toString()}) outputs.</li>
   * </ol>
   * So instances with the following values are considered <strong>equal and the same</strong>:
   * <ul>
   *   <li><code>foo</code></li>
   *   <li><code>FOO</code></li>
   * </ul>
   */
  @Nonnull
  protected static <TYPE extends OpenFlyweigth<TYPE>> TYPE of(@Nonnull final String value,
                                                              @Nonnull final ConcurrentMap<String, TYPE> knownFlyweights,
                                                              @Nonnull final Function<String, TYPE> creator) {
    final String key = requireNotEmptyTrimmed(value).toUpperCase(ENGLISH);
    return knownFlyweights.computeIfAbsent(key, creator);
  }

}
