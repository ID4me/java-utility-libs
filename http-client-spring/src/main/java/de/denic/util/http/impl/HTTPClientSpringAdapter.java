/*^
  ===========================================================================
  ID4me Utility Libs
  ===========================================================================
  Copyright (C) 2020 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.util.http.impl;

import de.denic.util.http.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestClientResponseException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;
import javax.annotation.concurrent.NotThreadSafe;
import java.net.URI;
import java.util.Map;

import static de.denic.util.http.HTTPClient.replaceVerticalWhiteSpaces;
import static de.denic.util.http.HTTPClient.shortenedBody;
import static java.util.Objects.requireNonNull;
import static java.util.Objects.requireNonNullElse;
import static org.springframework.http.RequestEntity.get;
import static org.springframework.http.RequestEntity.post;

@Immutable
public final class HTTPClientSpringAdapter implements HTTPClient {

  private static final Logger LOG = LoggerFactory.getLogger(HTTPClientSpringAdapter.class);

  private final RestTemplate restTemplate;

  public HTTPClientSpringAdapter(@Nonnull final RestTemplate restTemplate) {
    this.restTemplate = requireNonNull(restTemplate, "Missing REST template");
  }

  @Nonnull
  @Override
  public String responseBodyQuerying(@Nonnull final URI wellKnownURI,
                                     @Nullable final Map<String, Object> optQueryParameters) throws HTTPException {
    final URI targetURI;
    if (optQueryParameters == null || optQueryParameters.isEmpty()) {
      targetURI = wellKnownURI;
    } else {
      final UriComponentsBuilder targetURIBuilder = UriComponentsBuilder.fromUri(requireNonNull(wellKnownURI, "Missing well-known URI"));
      optQueryParameters.forEach(targetURIBuilder::queryParam);
      targetURI = targetURIBuilder.build().toUri();
    }
    final RequestEntity<?> getRequest = get(targetURI).build();
    return executeBlocking("Queried", getRequest, "Querying");
  }

  @Nonnull
  @Override
  public RequestParamSetter sendingTo(@Nonnull final URI targetURI) {
    return new RequestBuilderImpl(targetURI);
  }

  @Nonnull
  private String executeBlocking(@Nonnull final String successLogMessagePrefix,
                                 @Nonnull final RequestEntity<?> request,
                                 @Nonnull final String failureLogMessagePrefix) {
    try {
      final String responseBody = requireNonNullElse(restTemplate.exchange(request, String.class).getBody(), "");
      if (LOG.isDebugEnabled()) {
        LOG.debug("{} '{}': '{}'", successLogMessagePrefix, request.getUrl(), replaceVerticalWhiteSpaces(responseBody));
      }
      return responseBody;
    } catch (final RestClientResponseException e) {
      final int httpStatusCode = e.getRawStatusCode();
      final String httpStatusText = e.getStatusText();
      final String responseBody = e.getResponseBodyAsString();
      if (LOG.isDebugEnabled()) {
        LOG.debug("{} '{}' failed. Response code '{} {}', body: '{}'",
                failureLogMessagePrefix, request.getUrl(), httpStatusCode, httpStatusText, shortenedBody(responseBody));
      }
      throw new HTTPResponseException(httpStatusCode, httpStatusText, responseBody);
    } catch (final RestClientException e) {
      LOG.info("{} '{}' failed: {} ({})", failureLogMessagePrefix, request.getUrl(), e.getMessage(), e.getClass().getName());
      throw new HTTPException(e.getMessage());
    } catch (final RuntimeException e) {
      LOG.warn("{} '{}' failed.", failureLogMessagePrefix, request.getUrl(), e);
      throw e;
    }
  }

  @Nonnull
  @Override
  public URIBuilder uriBuilderOf(@Nonnull final URI initialURI) {
    return new URIBuilderSpringAdapter(initialURI);
  }

  @NotThreadSafe
  private class RequestBuilderImpl implements RequestParamSetter, RequestParamSetter.RequestSender {

    private final URI targetURI;
    private MediaType mediaType;

    RequestBuilderImpl(final URI targetURI) {
      this.targetURI = requireNonNull(targetURI, "Missing target URI");
    }

    @Nonnull
    @Override
    public RequestParamSetter.RequestSender asMediaType(@Nonnull final String mediaType) {
      this.mediaType = MediaType.parseMediaType(requireNonNull(mediaType, "Missing media type"));
      return this;
    }

    @Nonnull
    @Override
    public String responseBodyAfterPOSTing(@Nonnull final String requestBody) throws HTTPException {
      final RequestEntity<String> request = post(targetURI)
              .contentType(mediaType)
              .body(requestBody);
      return executeBlocking("POSTed to", request, "POSTing to");
    }

  }

}
