/*^
  ===========================================================================
  ID4me Utility Libs
  ===========================================================================
  Copyright (C) 2020 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.util.wellknown.openidconfig.impl;

import de.denic.util.http.HTTPResponseBodyToJSONConverter;
import de.denic.util.wellknown.openidconfig.OpenIDConfigClient;
import de.denic.util.wellknown.openidconfig.OpenIDConfigParsingException;

import javax.annotation.concurrent.Immutable;
import java.net.URI;

@Immutable
final class ResponseConverterImpl extends HTTPResponseBodyToJSONConverter.Base<OpenIDConfigParsingException>
        implements OpenIDConfigClient.ResponseConverter {

  ResponseConverterImpl(final String response, final URI targetURI) {
    super(response, targetURI, "Queried for OpenID Configuration",
            (exception, type) -> new OpenIDConfigParsingException("Parsing OpenID Configuration response from '" + targetURI + "' as type '" + type.getName() + "' failed.", exception));
  }

}
