/*^
  ===========================================================================
  ID4me Utility Libs
  ===========================================================================
  Copyright (C) 2020 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.util.wellknown.openidconfig;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import de.denic.util.wellknown.MissingPropertyException;

import javax.annotation.Nonnull;
import javax.annotation.concurrent.Immutable;
import java.net.URI;
import java.util.Collection;
import java.util.Optional;
import java.util.Set;

import static java.util.Collections.emptyList;
import static java.util.Objects.requireNonNull;
import static java.util.Objects.requireNonNullElse;
import static java.util.Optional.ofNullable;
import static java.util.stream.Collectors.toUnmodifiableSet;

/**
 * Represents (subset of) items from <a href="https://openid.net/specs/openid-connect-discovery-1_0.html#ProviderMetadata">OpenID Connect Discovery</a>.
 */
@JsonDeserialize(as = OpenIDConfig.Impl.class)
public interface OpenIDConfig {

  @Nonnull
  URI getIssuer();

  @Nonnull
  URI getAuthzEndpointURI();

  @Nonnull
  URI getJwkSetURI();

  @Nonnull
  Optional<URI> getOptFederationRegistrationEndpointURI();

  @Nonnull
  URI getFederationRegistrationEndpointURI() throws MissingPropertyException;

  @Nonnull
  Set<ClientRegistrationType> getSupportedClientRegistrationTypes();

  @Immutable
  @JsonIgnoreProperties(ignoreUnknown = true)
  final class Impl implements OpenIDConfig {

    private static final Collection<String> EMPTY_COLLECTION_OF_STRINGS = emptyList();

    private final URI issuer;
    private final URI jwkSetURI, authzEndpointURI;
    private final URI optFederationRegistrationEndpointURI;
    private final Set<ClientRegistrationType> fixedSupportedClientRegistrationTypes;

    /**
     * Intentionally missing null-safety annotations on params to let the code itself complain about errors more precisely.
     */
    public Impl(@JsonProperty("issuer") final URI issuer,
                @JsonProperty("authorization_endpoint") final URI authzEndpointURI,
                @JsonProperty("jwks_uri") final URI jwkSetURI,
                @JsonProperty("federation_registration_endpoint") final URI optFederationRegistrationEndpointURI,
                @JsonProperty("client_registration_types_supported") final Collection<String> supportedClientRegistrationTypes) {
      this.issuer = requireNonNull(issuer, "Missing JSON key 'issuer'");
      this.authzEndpointURI = requireNonNull(authzEndpointURI, "Missing JSON key 'authorization_endpoint'");
      this.jwkSetURI = requireNonNull(jwkSetURI, "Missing JSON key 'jwks_uri'");
      this.optFederationRegistrationEndpointURI = optFederationRegistrationEndpointURI;
      this.fixedSupportedClientRegistrationTypes = requireNonNullElse(supportedClientRegistrationTypes, EMPTY_COLLECTION_OF_STRINGS).stream()
              .map(ClientRegistrationType::of)
              .collect(toUnmodifiableSet());
    }

    @Nonnull
    @Override
    public URI getIssuer() {
      return issuer;
    }

    @Nonnull
    @Override
    public URI getAuthzEndpointURI() {
      return authzEndpointURI;
    }

    @Nonnull
    @Override
    public URI getJwkSetURI() {
      return jwkSetURI;
    }

    @Nonnull
    @Override
    public Optional<URI> getOptFederationRegistrationEndpointURI() {
      return ofNullable(optFederationRegistrationEndpointURI);
    }

    @Nonnull
    @Override
    public URI getFederationRegistrationEndpointURI() throws MissingPropertyException {
      if (optFederationRegistrationEndpointURI == null) {
        throw new MissingPropertyException("OpenID Configuration", issuer, "Federation Registration Endpoint URI", "federation_registration_endpoint");
      }

      return optFederationRegistrationEndpointURI;
    }

    @Nonnull
    @Override
    public Set<ClientRegistrationType> getSupportedClientRegistrationTypes() {
      return fixedSupportedClientRegistrationTypes;
    }

    @Override
    public boolean equals(final Object o) {
      if (this == o)
        return true;
      if (o == null || getClass() != o.getClass())
        return false;

      final Impl that = (Impl) o;

      if (!issuer.equals(that.issuer))
        return false;
      if (!authzEndpointURI.equals(that.authzEndpointURI))
        return false;
      if (!jwkSetURI.equals(that.jwkSetURI))
        return false;
      if (!fixedSupportedClientRegistrationTypes.equals(that.fixedSupportedClientRegistrationTypes))
        return false;
      return optFederationRegistrationEndpointURI != null ? optFederationRegistrationEndpointURI.equals(that.optFederationRegistrationEndpointURI) : that.optFederationRegistrationEndpointURI == null;
    }

    @Override
    public int hashCode() {
      int result = issuer.hashCode();
      result = 31 * result + authzEndpointURI.hashCode();
      result = 31 * result + jwkSetURI.hashCode();
      result = 31 * result + (optFederationRegistrationEndpointURI != null ? optFederationRegistrationEndpointURI.hashCode() : 0);
      result = 31 * result + fixedSupportedClientRegistrationTypes.hashCode();
      return result;
    }

    @Override
    public String toString() {
      return "{issuer=" + issuer +
              ", authzEndpURI=" + authzEndpointURI +
              ", jwkSetURI=" + jwkSetURI +
              ", fedRegEndp=" + optFederationRegistrationEndpointURI +
              ", clientRegTypes=" + fixedSupportedClientRegistrationTypes +
              '}';
    }

  }

}