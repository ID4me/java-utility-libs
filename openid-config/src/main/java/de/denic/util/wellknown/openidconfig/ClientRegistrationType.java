/*^
  ===========================================================================
  ID4me Utility Libs
  ===========================================================================
  Copyright (C) 2020 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.util.wellknown.openidconfig;

import de.denic.util.flyweight.OpenFlyweigth;

import javax.annotation.Nonnull;
import javax.annotation.concurrent.Immutable;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

@Immutable
public final class ClientRegistrationType extends OpenFlyweigth<ClientRegistrationType> {

  private static final ConcurrentMap<String, ClientRegistrationType> FLYWEIGHTS = new ConcurrentHashMap<>();

  public static final ClientRegistrationType DYNAMIC = of("DYNAMIC");
  /**
   * The following two instances are declared in
   * <a href="https://openid.net/specs/openid-connect-federation-1_0.html#rfc.section.3.1">OpenID Connect Federation 1.0 - draft 12</a>.
   */
  public static final ClientRegistrationType AUTOMATIC = of("AUTOMATIC");
  public static final ClientRegistrationType EXPLICIT = of("EXPLICIT");

  private ClientRegistrationType(@Nonnull final String value) {
    super(value);
  }

  @Nonnull
  public static ClientRegistrationType of(@Nonnull final String value) {
    return OpenFlyweigth.of(value, FLYWEIGHTS, ClientRegistrationType::new);
  }

}
