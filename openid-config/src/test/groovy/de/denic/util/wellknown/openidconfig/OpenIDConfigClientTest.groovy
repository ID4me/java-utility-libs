/*^
  ===========================================================================
  ID4me Utility Libs
  ===========================================================================
  Copyright (C) 2020 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.util.wellknown.openidconfig

import de.denic.util.http.HTTPClient
import de.denic.util.http.HTTPResponseException
import de.denic.util.http.impl.URIBuilderMicronautAdapter
import de.denic.util.wellknown.openidconfig.impl.OpenIDConfigClientImpl
import spock.lang.Specification

import static de.denic.util.http.HTTPResponseException.NOT_FOUND

class OpenIDConfigClientTest extends Specification {

  HTTPClient httpClientMock = Mock()
  OpenIDConfigClient CUT

  def setup() {
    CUT = new OpenIDConfigClientImpl(httpClientMock)
  }

  def 'No fallback to RFC8615-incompliant Well-Known with path-less target URI'() {
    given:
    def noPathURI = URI.create('http://no.path.uri')

    when:
    CUT.openIDConfigOf(noPathURI)

    then:
    1 * httpClientMock.uriBuilderOf(noPathURI)
            >> new URIBuilderMicronautAdapter(noPathURI)
    1 * httpClientMock.responseBodyQuerying(URI.create('http://no.path.uri/.well-known/openid-configuration'))
            >> { throw new HTTPResponseException(NOT_FOUND, 'NOT found', 'Response body') }
    def exception = thrown(HTTPResponseException)
    exception.statusCode == NOT_FOUND
  }

  def 'No fallback to RFC8615-incompliant Well-Known target URI with HTTP response status NOT 404'() {
    given:
    def uriWithPath = URI.create('http://uri.with/path')

    when:
    CUT.openIDConfigOf(uriWithPath)

    then:
    1 * httpClientMock.uriBuilderOf(uriWithPath)
            >> new URIBuilderMicronautAdapter(uriWithPath)
    1 * httpClientMock.responseBodyQuerying(URI.create('http://uri.with/.well-known/openid-configuration/path'))
            >> { throw new HTTPResponseException(400, 'BAD request', 'Response body') }
    def exception = thrown(HTTPResponseException)
    exception.statusCode == 400
  }

  def 'Successful fallback to RFC8615-incompliant Well-Known URI'() {
    given:
    def uriWithPath = URI.create('http://uri.with/path')
    def responseBody = '{"some":"json"}'

    when:
    def responseConverter = CUT.openIDConfigOf(uriWithPath)

    then:
    1 * httpClientMock.uriBuilderOf(uriWithPath)
            >> new URIBuilderMicronautAdapter(uriWithPath)
    1 * httpClientMock.responseBodyQuerying(URI.create('http://uri.with/.well-known/openid-configuration/path'))
            >> { throw new HTTPResponseException(NOT_FOUND, 'NOT found', 'Response body') }
    1 * httpClientMock.uriBuilderOf(uriWithPath)
            >> new URIBuilderMicronautAdapter(uriWithPath)
    1 * httpClientMock.responseBodyQuerying(URI.create('http://uri.with/path/.well-known/openid-configuration'))
            >> responseBody
    responseConverter.asJsonNode().toString() == responseBody
  }

  def 'Fallback to RFC8615-incompliant Well-Known URI fails also'() {
    given:
    def uriWithPath = URI.create('http://uri.with/path')

    when:
    CUT.openIDConfigOf(uriWithPath)

    then:
    1 * httpClientMock.uriBuilderOf(uriWithPath)
            >> new URIBuilderMicronautAdapter(uriWithPath)
    1 * httpClientMock.responseBodyQuerying(URI.create('http://uri.with/.well-known/openid-configuration/path'))
            >> { throw new HTTPResponseException(NOT_FOUND, 'NOT found', 'Response body') }
    1 * httpClientMock.uriBuilderOf(uriWithPath)
            >> new URIBuilderMicronautAdapter(uriWithPath)
    1 * httpClientMock.responseBodyQuerying(URI.create('http://uri.with/path/.well-known/openid-configuration'))
            >> { throw new HTTPResponseException(NOT_FOUND, 'NOT found', 'Response body') }
    def exception = thrown(HTTPResponseException)
    exception.statusCode == NOT_FOUND
  }

}
