/*^
  ===========================================================================
  ID4me Utility Libs
  ===========================================================================
  Copyright (C) 2020 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.util.wellknown.openidconfig

import com.fasterxml.jackson.databind.JsonNode
import de.denic.util.http.HTTPResponseException
import de.denic.util.http.impl.HTTPClientMicronautAdapter
import de.denic.util.wellknown.MissingPropertyException
import de.denic.util.wellknown.openidconfig.impl.OpenIDConfigClientImpl
import io.micronaut.http.client.HttpClient
import io.micronaut.test.annotation.MicronautTest
import spock.lang.Specification

import javax.inject.Inject

import static de.denic.util.wellknown.openidconfig.ClientRegistrationType.EXPLICIT

@MicronautTest
class OpenIDConfigClientIntegrationTest extends Specification {

  static final URI ACCOUNT_GOOGLE_COM = URI.create('https://accounts.google.com')

  @Inject
  HttpClient httpClient
  OpenIDConfigClient CUT

  def setup() {
    CUT = new OpenIDConfigClientImpl(new HTTPClientMicronautAdapter(httpClient))
  }

  def 'Querying Well-Known OpenID Configuration of "id.denic.de"'() {
    given:
    def idDenicDe = URI.create('https://id.denic.de')

    when:
    OpenIDConfig openIDConfig = CUT.openIDConfigOf(idDenicDe).as(OpenIDConfig)

    then:
    with(openIDConfig) {
      it.issuer == idDenicDe
      it.authzEndpointURI.toASCIIString() == 'https://id.denic.de/login'
      it.jwkSetURI.toASCIIString() == 'https://id.denic.de/jwks.json'
      it.federationRegistrationEndpointURI.toASCIIString() == 'https://id.denic.de/federation/clients'
      it.supportedClientRegistrationTypes.contains(EXPLICIT)
    }
  }

  def 'Querying not-available representation of Well-Known OpenID Configuration'() {
    given:
    def exampleCom = URI.create('http://example.com')

    when:
    CUT.openIDConfigOf(exampleCom)

    then:
    thrown(HTTPResponseException)

    expect:
    CUT.optOpenIDConfigOf(exampleCom).isEmpty()
  }

  def 'Querying for JsonNode representation of Well-Known OpenID Configuration'() {
    expect:
    JsonNode jsonNode = CUT.openIDConfigOf(ACCOUNT_GOOGLE_COM).asJsonNode()
    jsonNode.get('issuer').asText() == 'https://accounts.google.com'
  }

  def 'Querying for correctly typed representation of Well-Known OpenID Configuration'() {
    when:
    def responseConverter = CUT.openIDConfigOf(ACCOUNT_GOOGLE_COM)
    OpenIDConfig openIDConfig = responseConverter.as(OpenIDConfig)
    println "Parsed OpenID Config of ${ACCOUNT_GOOGLE_COM}: $openIDConfig"

    then:
    with(openIDConfig) {
      it.issuer.toASCIIString() == 'https://accounts.google.com'
      it.authzEndpointURI != null
      it.jwkSetURI != null
      it.optFederationRegistrationEndpointURI.isEmpty()
    }

    when:
    openIDConfig.federationRegistrationEndpointURI

    then:
    def missingPropertyException = thrown(MissingPropertyException)
    println "Caught: ${missingPropertyException}"

    when:
    Optional<OpenIDConfig> optOpenIDConfig = responseConverter.asOpt(OpenIDConfig)

    then:
    with(optOpenIDConfig) {
      it.isPresent()
      it.get() == openIDConfig
    }
  }

  def 'Querying for mismatching type of representation of Well-Known OpenID Configuration'() {
    when:
    def openIDConfigConverter = CUT.openIDConfigOf(ACCOUNT_GOOGLE_COM)
    openIDConfigConverter.as(URI)

    then:
    thrown(OpenIDConfigParsingException)

    expect:
    openIDConfigConverter.asOpt(URI).isEmpty()
  }

}
