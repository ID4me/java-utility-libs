/*^
  ===========================================================================
  ID4me Utility Libs
  ===========================================================================
  Copyright (C) 2020 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.util.wellknown.openidfederation

import com.fasterxml.jackson.core.JsonProcessingException
import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.ObjectReader
import com.fasterxml.jackson.databind.ObjectWriter
import com.fasterxml.jackson.databind.node.JsonNodeFactory
import com.nimbusds.jose.jwk.Curve
import com.nimbusds.jose.jwk.ECKey
import com.nimbusds.jose.jwk.JWKSet
import com.nimbusds.jose.jwk.gen.ECKeyGenerator
import spock.lang.Specification

import java.time.Instant

class FederationEntityConfigTest extends Specification {

  // Found here: https://openid.net/specs/openid-connect-federation-1_0.html#rfc.section.5.2
  static final String EXAMPLE_ENTITY_CONFIG_FROM_SPEC = '''
        {
          "iss": "https://example.com",
          "sub": "https://example.com",
          "iat": 1516239022,
          "exp": 1516298022,
          "metadata": {
            "federation_entity": {
              "federation_api_endpoint":
                "https://example.com/federation_api_endpoint",
              "name": "The example cooperation",
              "homepage_uri": "https://www.example.com"
            }
          },
          "authority_hints": ["https://federation.example.com"],
          "jwks": {
            "keys": [
              {
                "alg": "RS256",
                "e": "AQAB",
                "ext": true,
                "key_ops": [
                  "verify"
                ],
                "kid": "key1",
                "kty": "RSA",
                "n": "pnXBOusEANuug6ewezb9J_...",
                "use": "sig"
              }
            ]
          }
        }'''
  static final ObjectMapper OBJECT_MAPPER = new ObjectMapper()
  static final ObjectReader FEDERATION_ENTITY_CONFIG_READER = OBJECT_MAPPER.readerFor(FederationEntityConfig)
  static final ObjectWriter FEDERATION_ENTITY_CONFIG_WRITER = OBJECT_MAPPER.writerFor(FederationEntityConfig)

  def 'Unmarshalling example Entity Configuration from spec'() {
    when:
    FederationEntityConfig unmarshalled = FEDERATION_ENTITY_CONFIG_READER.readValue(EXAMPLE_ENTITY_CONFIG_FROM_SPEC)

    then:
    unmarshalled.issuer.toString() == 'https://example.com'
    unmarshalled.subject.toString() == 'https://example.com'
    unmarshalled.isSelfSigned()
    !unmarshalled.isValidNow()
    unmarshalled.isValidAt(Instant.parse('2018-01-18T12:00:00Z'))
    unmarshalled.optFederationApiEndpoint.get() == URI.create('https://example.com/federation_api_endpoint')
    unmarshalled.authorityHints == Set.of(URI.create('https://federation.example.com'))
  }

  def "FAILURE unmarshalling JSON missing 'jwks' key"() {
    given:
    def jsonToParse = '''
        {
          "iss": "https://example.com",
          "sub": "https://example.com",
          "iat": 1516239022,
          "exp": 1516298022
        }'''

    when:
    FEDERATION_ENTITY_CONFIG_READER.readValue(jsonToParse)

    then:
    thrown(JsonProcessingException)
  }

  def "Unmarshalling JSON missing 'federation_api_endpoint' key yields no endpoint value"() {
    given:
    def jsonToParse = '''
        {
          "iss": "https://example.com",
          "sub": "https://example.com",
          "iat": 1516239022,
          "exp": 1516298022,
          "metadata": {
            "federation_entity": {
              "name": "The example cooperation",
              "homepage_uri": "https://www.example.com"
            }
          },
          "jwks": {
            "keys": [
              {
                "alg": "RS256",
                "e": "AQAB",
                "ext": true,
                "key_ops": [
                  "verify"
                ],
                "kid": "key1",
                "kty": "RSA",
                "n": "pnXBOusEANuug6ewezb9J_...",
                "use": "sig"
              }
            ]
          }
        }'''

    expect:
    FEDERATION_ENTITY_CONFIG_READER.readValue(jsonToParse).optFederationApiEndpoint.isEmpty()
  }

  def 'Marshalling roundtrip of an artificial Entity Configuration'() {
    given:
    ECKey jwk = new ECKeyGenerator(Curve.P_256)
            .keyID("test-key")
            .generate()
    URI issuer = URI.create('http://entity.uri')
    long issuedAt = System.currentTimeSeconds()
    long expiresAt = issuedAt + 100_000L
    JWKSet jwkSet = new JWKSet(jwk.toPublicJWK())
    def metaDataNode = JsonNodeFactory.instance.objectNode()
    metaDataNode.putObject('federation_entity')
            .put('federation_api_endpoint', "${issuer}/federation-api-endpoint")
    JsonNode constraintsNode = null
    Collection<URI> authorityHints = [URI.create('http://authority.hint')]
    FederationEntityConfig CUT = new FederationEntityConfig.Impl(issuer, issuedAt, expiresAt, jwkSet, metaDataNode, constraintsNode, authorityHints)
    assert CUT.optFederationApiEndpoint.isPresent()

    when: 'JSON serialisation'
    def serializedCUT = FEDERATION_ENTITY_CONFIG_WRITER.writeValueAsString(CUT)

    then:
    println "Received JSON-serialised CUT: ${serializedCUT}"
    !serializedCUT.isEmpty()

    when: 'JSON deserialisation'
    FederationEntityConfig deserialisedCUT = FEDERATION_ENTITY_CONFIG_READER.readValue(serializedCUT)

    then:
    CUT == deserialisedCUT
    deserialisedCUT.optFederationApiEndpoint.isPresent()
  }

}
