/*^
  ===========================================================================
  ID4me Utility Libs
  ===========================================================================
  Copyright (C) 2020 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.util.wellknown.openidfederation.impl

import spock.lang.Specification

import java.time.ZonedDateTime

import static java.time.Instant.EPOCH

class ValidityConstrainedTest extends Specification {

  def "Without expiration time instance is valid (roughly) forever"() {
    given:
    def now = ZonedDateTime.now()
    def notExpiring = new ValidityConstrained.Impl(now.minusHours(1L), null)

    expect:
    !notExpiring.isValidAt(EPOCH)
    !notExpiring.isValidAt(now.minusHours(2L))
    notExpiring.isValidAt(notExpiring.getIssuedAt())
    notExpiring.isValidNow()
    notExpiring.isValidAt(now.plusYears(1_000_000L))
    notExpiring.getExpiresAt().isEmpty()
  }

  def "With expiration time instance is NOT valid forever"() {
    given:
    def now = ZonedDateTime.now()
    def expiresInOneHour = new ValidityConstrained.Impl(now.minusHours(1L), now.plusHours(1L))

    expect:
    !expiresInOneHour.isValidAt(EPOCH)
    !expiresInOneHour.isValidAt(now.minusHours(2L))
    expiresInOneHour.isValidAt(expiresInOneHour.getIssuedAt())
    expiresInOneHour.isValidNow()
    expiresInOneHour.isValidAt(now.plusMinutes(59L).plusSeconds(59L))
    !expiresInOneHour.isValidAt(now.plusHours(1L))
    !expiresInOneHour.isValidAt(now.plusYears(1_000_000L))
    expiresInOneHour.getExpiresAt().isPresent()
  }

}
