/*^
  ===========================================================================
  ID4me Utility Libs
  ===========================================================================
  Copyright (C) 2020 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.util.wellknown.openidfederation

import com.fasterxml.jackson.databind.ObjectMapper
import spock.lang.Specification

import static java.time.temporal.ChronoField.INSTANT_SECONDS

class ExplicitClientRegistrationResponseTest extends Specification {

  private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper()
  private static final String CUT_SERIALIZED_AS_JSON = '''{
        "iss"             : "http://issuer.uri",
        "sub"             : "http://subject.uri",
        "aud"             : "http://subject.uri",
        "iat"             : 1594365784,
        "exp"             : 1594365843,
        "authority_hints" : [ "http://authority.hint" ],
        "metadata_policy" : {
            "openid_relying_party" : {
                "client_id"                    : { "value" : "Client123" },
                "client_id_issued_at"          : { "value" : 1594030600 },
                "client_secret"                : { "value" : "ClientVerySecretClientSecret" },
                "client_secret_expires_at"     : { "value" : 1994635400 },
                "grant_types"                  : { "value" : [ "authorization_code" ] },
                "response_types"               : { "value" : [ "code" ] },
                "token_endpoint_auth_method"   : { "value" : "client_secret_basic" },
                "id_token_signed_response_alg" : { "value" : "RS256" }
            }
        }
}'''

  def 'JSON unmarshalling of instance with expiring client secret'() {
    when:
    ExplicitClientRegistrationResponse CUT = OBJECT_MAPPER.readerFor(ExplicitClientRegistrationResponse).readValue(CUT_SERIALIZED_AS_JSON)

    then:
    println CUT
    CUT.getIssuer().toASCIIString() == 'http://issuer.uri'
    CUT.getSubject().toASCIIString() == 'http://subject.uri'
    with(CUT.getPublicJWKSetOfSubject()) {
      it.getKeys().isEmpty()
    }
    with(CUT.getRelyingPartyMetaDataPolicy()) {
      it.clientID == 'Client123'
      it.idIssuedAt.getLong(INSTANT_SECONDS) == 1594030600L
      it.clientSecret == 'ClientVerySecretClientSecret'
      it.secretExpiresAt.get().getLong(INSTANT_SECONDS) == 1994635400L
    }
    with(CUT.getAuthorityHints()) {
      it.size() == 1
      it.contains(URI.create('http://authority.hint'))
    }
  }

  def 'JSON unmarshalling of instance with not-expiring client secret'() {
    given:
    def notExpiringSerializedJSON = CUT_SERIALIZED_AS_JSON.replace('"value" : 1994635400', '"value" : 0')

    when:
    ExplicitClientRegistrationResponse CUT = OBJECT_MAPPER.readerFor(ExplicitClientRegistrationResponse).readValue(notExpiringSerializedJSON)

    then:
    println CUT
    CUT.getIssuer().toASCIIString() == 'http://issuer.uri'
    CUT.getSubject().toASCIIString() == 'http://subject.uri'
    with(CUT.getPublicJWKSetOfSubject()) {
      it.getKeys().isEmpty()
    }
    with(CUT.getRelyingPartyMetaDataPolicy()) {
      it.clientID == 'Client123'
      it.idIssuedAt.getLong(INSTANT_SECONDS) == 1594030600L
      it.clientSecret == 'ClientVerySecretClientSecret'
      it.secretExpiresAt.isEmpty()
    }
    with(CUT.getAuthorityHints()) {
      it.size() == 1
      it.contains(URI.create('http://authority.hint'))
    }
  }

}
