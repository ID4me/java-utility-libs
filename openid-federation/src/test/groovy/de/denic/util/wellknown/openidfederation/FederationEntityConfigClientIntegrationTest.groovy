/*^
  ===========================================================================
  ID4me Utility Libs
  ===========================================================================
  Copyright (C) 2020 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.util.wellknown.openidfederation

import com.fasterxml.jackson.databind.JsonNode
import de.denic.util.http.HTTPResponseException
import de.denic.util.http.impl.HTTPClientMicronautAdapter
import de.denic.util.jws.JWSOf
import de.denic.util.jws.JWSOfSelfSigned
import de.denic.util.wellknown.openidfederation.impl.FederationEntityConfigClientImpl
import io.micronaut.http.client.HttpClient
import io.micronaut.test.annotation.MicronautTest
import spock.lang.Specification

import javax.inject.Inject

@MicronautTest
class FederationEntityConfigClientIntegrationTest extends Specification {

  static final URI ID_DENIC_DE = URI.create('https://id.denic.de')

  @Inject
  HttpClient httpClient
  FederationEntityConfigClient CUT

  def setup() {
    CUT = new FederationEntityConfigClientImpl(new HTTPClientMicronautAdapter(httpClient))
  }

  def 'Querying not-available representation of Well-Known Federation Entity Configuration'() {
    given:
    def exampleCom = URI.create('http://example.com')

    when:
    CUT.federationEntityConfigOf(exampleCom)

    then:
    thrown(HTTPResponseException)

    expect:
    CUT.optFederationEntityConfigOf(exampleCom).isEmpty()
  }

  def 'Querying for JsonNode representation of Well-Known Federation Entity Configuration'() {
    when:
    def responseConverter = CUT.federationEntityConfigOf(ID_DENIC_DE)

    then:
    JsonNode jsonNode = responseConverter.asJsonNodeOfPayload()
    jsonNode.get('iss').asText() == 'https://id.denic.de'
    def optJsonNode = responseConverter.asOptJsonNodeOfPayload()
    optJsonNode.isPresent()
    optJsonNode.get() == jsonNode
  }

  def 'Querying for correctly JWS-typed representation of Well-Known Federation Entity Configuration'() {
    when:
    def responseConverter = CUT.federationEntityConfigOf(ID_DENIC_DE)
    JWSOf<FederationEntityConfig> jwsOfFederationEntityConfig = responseConverter.asJWSOf(FederationEntityConfig)

    then:
    println "Parsed Federation Entity Config of ${ID_DENIC_DE}: $jwsOfFederationEntityConfig"
    with(jwsOfFederationEntityConfig) {
      it.verifiableWith(it.payload.publicJWKSetOfSubject)
      with(it.payload) {
        it.issuer.toASCIIString() == 'https://id.denic.de'
        it.isSelfSigned()
        it.isValidNow()
      }
    }

    when:
    Optional<JWSOf<FederationEntityConfig>> optJWSOfFederationEntityConfig = responseConverter.asOptJWSOf(FederationEntityConfig)

    then:
    with(optJWSOfFederationEntityConfig) {
      it.isPresent()
      it.get() == jwsOfFederationEntityConfig
    }
  }

  def 'Querying for mismatching type of representation of Well-Known Federation Entity Configuration'() {
    when:
    CUT.federationEntityConfigOf(ID_DENIC_DE).asJWSOf(URI)

    then:
    def caught = thrown(FederationEntityConfigParsingException)
    println "Caught: ${caught}"

    expect:
    CUT.federationEntityConfigOf(ID_DENIC_DE).asOptJWSOf(URI).isEmpty()
  }

  def 'Querying for correctly self-signed JWS-typed representation of Well-Known Federation Entity Configuration'() {
    when:
    def responseConverter = CUT.federationEntityConfigOf(ID_DENIC_DE)
    JWSOfSelfSigned<FederationEntityConfig> jwsOfFederationEntityConfig = responseConverter.asJWSOfSelfSigned(FederationEntityConfig)

    then:
    with(jwsOfFederationEntityConfig) {
      it.verifiableSelfSigned()
      with(it.payload) {
        it.issuer.toASCIIString() == 'https://id.denic.de'
      }
    }

    when:
    Optional<JWSOfSelfSigned<FederationEntityConfig>> optJWSOfFederationEntityConfig = responseConverter.asOptJWSOfSelfSigned(FederationEntityConfig)

    then:
    with(optJWSOfFederationEntityConfig) {
      it.isPresent()
      it.get() == jwsOfFederationEntityConfig
    }
  }

}
