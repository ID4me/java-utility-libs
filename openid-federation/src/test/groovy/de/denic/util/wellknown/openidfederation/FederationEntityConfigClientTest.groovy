/*^
  ===========================================================================
  ID4me Utility Libs
  ===========================================================================
  Copyright (C) 2020 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.util.wellknown.openidfederation

import com.nimbusds.jose.*
import com.nimbusds.jose.crypto.ECDSASigner
import com.nimbusds.jose.jwk.Curve
import com.nimbusds.jose.jwk.ECKey
import com.nimbusds.jose.jwk.gen.ECKeyGenerator
import de.denic.util.http.HTTPClient
import de.denic.util.http.HTTPResponseException
import de.denic.util.http.impl.URIBuilderMicronautAdapter
import de.denic.util.wellknown.openidfederation.impl.FederationEntityConfigClientImpl
import spock.lang.Specification

import static de.denic.util.http.HTTPResponseException.NOT_FOUND

class FederationEntityConfigClientTest extends Specification {

  static final String KEY_ID = 'test-key'
  static final ECKey JWK = new ECKeyGenerator(Curve.P_256)
          .keyID(KEY_ID)
          .generate()
  static final JWSSigner SIGNER = new ECDSASigner(JWK)

  HTTPClient httpClientMock = Mock()
  FederationEntityConfigClient CUT

  def setup() {
    CUT = new FederationEntityConfigClientImpl(httpClientMock)
  }

  def 'No fallback to RFC8615-incompliant Well-Known with path-less target URI'() {
    given:
    def noPathURI = URI.create('http://no.path.uri')

    when:
    CUT.federationEntityConfigOf(noPathURI)

    then:
    1 * httpClientMock.uriBuilderOf(noPathURI)
            >> new URIBuilderMicronautAdapter(noPathURI)
    1 * httpClientMock.responseBodyQuerying(URI.create("${noPathURI}/.well-known/openid-federation"))
            >> { throw new HTTPResponseException("Message", NOT_FOUND, "NOT FOUND") }
    def exception = thrown(HTTPResponseException)
    println "Caught: ${exception}"
    exception.statusCode == NOT_FOUND
  }

  def 'No fallback to RFC8615-incompliant Well-Known target URI with HTTP response status NOT 404'() {
    given:
    def uriWithPath = URI.create('http://uri.with/path')

    when:
    CUT.federationEntityConfigOf(uriWithPath)

    then:
    1 * httpClientMock.uriBuilderOf(uriWithPath)
            >> new URIBuilderMicronautAdapter(uriWithPath)
    1 * httpClientMock.responseBodyQuerying(URI.create('http://uri.with/.well-known/openid-federation/path'))
            >> { throw new HTTPResponseException("Message", 400, "BAD REQUEST") }
    def exception = thrown(HTTPResponseException)
    println "Caught: ${exception}"
    exception.statusCode == 400
  }

  def 'Fallback to RFC8615-incompliant Well-Known URI successful'() {
    given:
    def uriWithPath = URI.create('http://uri.with/path')
    def jwsPayload = '{"dummy":"response"}'
    JWSObject jws = new JWSObject(
            new JWSHeader.Builder(JWSAlgorithm.ES256).keyID(JWK.getKeyID()).build(),
            new Payload(jwsPayload))
    jws.sign(SIGNER)
    def responseBody = jws.serialize()

    when:
    def responseConverter = CUT.federationEntityConfigOf(uriWithPath)

    then:
    1 * httpClientMock.uriBuilderOf(uriWithPath)
            >> new URIBuilderMicronautAdapter(uriWithPath)
    1 * httpClientMock.responseBodyQuerying(URI.create('http://uri.with/.well-known/openid-federation/path'))
            >> { throw new HTTPResponseException("Message", NOT_FOUND, "NOT FOUND") }
    1 * httpClientMock.uriBuilderOf(uriWithPath)
            >> new URIBuilderMicronautAdapter(uriWithPath)
    1 * httpClientMock.responseBodyQuerying(URI.create('http://uri.with/path/.well-known/openid-federation'))
            >> responseBody
    responseConverter.asJsonNodeOfPayload().toString() == jwsPayload
  }

  def 'Fallback to RFC8615-incompliant Well-Known URI fails also'() {
    given:
    def uriWithPath = URI.create('http://uri.with/path')

    when:
    CUT.federationEntityConfigOf(uriWithPath)

    then:
    1 * httpClientMock.uriBuilderOf(uriWithPath)
            >> new URIBuilderMicronautAdapter(uriWithPath)
    1 * httpClientMock.responseBodyQuerying(URI.create('http://uri.with/.well-known/openid-federation/path'))
            >> { throw new HTTPResponseException("Message", NOT_FOUND, "NOT FOUND") }
    1 * httpClientMock.uriBuilderOf(uriWithPath)
            >> new URIBuilderMicronautAdapter(uriWithPath)
    1 * httpClientMock.responseBodyQuerying(URI.create('http://uri.with/path/.well-known/openid-federation'))
            >> { throw new HTTPResponseException("Message", NOT_FOUND, "NOT FOUND") }
    def exception = thrown(HTTPResponseException)
    println "Caught: ${exception}"
    exception.statusCode == NOT_FOUND
  }

}
