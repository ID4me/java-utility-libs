/*^
  ===========================================================================
  ID4me Utility Libs
  ===========================================================================
  Copyright (C) 2020 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.util.wellknown.openidfederation

import com.fasterxml.jackson.databind.JsonNode
import com.nimbusds.jose.jwk.JWKSet
import de.denic.util.http.HTTPResponseException
import de.denic.util.http.impl.HTTPClientMicronautAdapter
import de.denic.util.jws.JWSOf
import de.denic.util.wellknown.openidfederation.impl.FederationEntityStmtClientImpl
import io.micronaut.http.client.HttpClient
import io.micronaut.test.annotation.MicronautTest
import spock.lang.Specification

import javax.inject.Inject

@MicronautTest
class FederationEntityStmtClientIntegrationTest extends Specification {

  static final URI ID_STAGING_DENIC_DE = URI.create('https://id.staging.denic.de')
  static final URI DUMMY_TRUST_ANCHOR = URI.create('https://id.staging.denic.de/dummy/trust-anchor')

  @Inject
  HttpClient httpClient
  FederationEntityStmtClient CUT

  def setup() {
    CUT = new FederationEntityStmtClientImpl(new HTTPClientMicronautAdapter(httpClient))
  }

  def 'Querying for an issuer not having Well-Known Federation Entity Configuration'() {
    given:
    def exampleCom = URI.create('http://example.com')

    when:
    CUT.federationEntityStmtIssuedBy(exampleCom)
            .atIssuersResolvedEndpoint()

    then:
    thrown(HTTPResponseException)

    expect:
    CUT.federationEntityStmtIssuedBy(exampleCom)
            .optAtIssuersResolvedEndpoint().isEmpty()
  }

  def 'Querying for JsonNode representation of Federation Entity Statement'() {
    when:
    def responseConverter = CUT.federationEntityStmtIssuedBy(DUMMY_TRUST_ANCHOR)
            .atIssuersResolvedEndpoint()
            .regarding(ID_STAGING_DENIC_DE)

    then:
    JsonNode jsonNode = responseConverter.asJsonNodeOfPayload()
    jsonNode.get('iss').asText() == 'https://id.staging.denic.de/dummy/trust-anchor'
    jsonNode.get('sub').asText() == 'https://id.staging.denic.de'
    def optJsonNode = responseConverter.asOptJsonNodeOfPayload()
    optJsonNode.isPresent()
    optJsonNode.get() == jsonNode
  }

  def 'Querying for correctly JWS-typed representation of Well-Known Federation Entity Statement'() {
    when:
    def responseConverter = CUT.federationEntityStmtIssuedBy(DUMMY_TRUST_ANCHOR)
            .atIssuersResolvedEndpoint()
            .regarding(ID_STAGING_DENIC_DE)
    JWSOf<FederationEntityStmt> jwsOfFederationEntityStmt = responseConverter.asJWSOf(FederationEntityStmt)

    then:
    println "Parsed Federation Entity Stmt of '${DUMMY_TRUST_ANCHOR}' regarding '${ID_STAGING_DENIC_DE}': $jwsOfFederationEntityStmt"
    JWKSet publicJWKSetOfIssuer = responseConverter.optFederationEntityConfigOfIssuer()
            .map(config -> config.publicJWKSetOfSubject)
            .orElseThrow()
    with(jwsOfFederationEntityStmt) {
      it.verifiableWith(publicJWKSetOfIssuer)
      with(it.payload) {
        it.issuer.toASCIIString() == 'https://id.staging.denic.de/dummy/trust-anchor'
        !it.isSelfSigned()
        it.isValidNow()
      }
    }

    when:
    Optional<JWSOf<FederationEntityStmt>> optJWSOfFederationEntityStmt = responseConverter.asOptJWSOf(FederationEntityStmt)

    then:
    with(optJWSOfFederationEntityStmt) {
      it.isPresent()
      it.get() == jwsOfFederationEntityStmt
    }
  }

  def 'Querying for mismatching type of representation of Well-Known Federation Entity Statement'() {
    given:
    def responseConverter = CUT.federationEntityStmtIssuedBy(DUMMY_TRUST_ANCHOR)
            .atIssuersResolvedEndpoint()
            .regarding(ID_STAGING_DENIC_DE)

    when:
    responseConverter.asJWSOf(URI)

    then:
    def caught = thrown(FederationEntityStmtParsingException)
    println "Caught: ${caught}"

    expect:
    responseConverter.asOptJWSOf(URI).isEmpty()
  }

}
