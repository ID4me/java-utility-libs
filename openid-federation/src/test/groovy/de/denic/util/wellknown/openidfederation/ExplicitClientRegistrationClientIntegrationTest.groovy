/*^
  ===========================================================================
  ID4me Utility Libs
  ===========================================================================
  Copyright (C) 2020 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.util.wellknown.openidfederation

import com.nimbusds.jose.jwk.ECKey
import com.nimbusds.jose.jwk.JWKSet
import de.denic.util.http.impl.HTTPClientMicronautAdapter
import de.denic.util.jws.JWSOfSelfSigned
import de.denic.util.wellknown.openidfederation.impl.ClientRegistrationClientImpl
import io.micronaut.http.client.HttpClient
import io.micronaut.test.annotation.MicronautTest
import spock.lang.Ignore
import spock.lang.Specification

import javax.inject.Inject
import java.time.ZonedDateTime

import static java.time.format.DateTimeFormatter.ofPattern
import static java.time.temporal.ChronoUnit.YEARS

@MicronautTest
class ExplicitClientRegistrationClientIntegrationTest extends Specification {

  @Inject
  HttpClient httpClient
  ClientRegistrationClient CUT

  def setup() {
    CUT = new ClientRegistrationClientImpl(new HTTPClientMicronautAdapter(httpClient))
  }

  def cleanup() {
    httpClient.stop()
  }

  /**
   * You have to use the data (URIs, JWK, Key-ID etc.) of real-life Relying Party 'rp.test.denic.de', cause C2id server will check
   * <ul>
   *   <li>Well-known OpenID Federation endpoint '/.well-known/openid-federation'</li>
   *   <li>Authority hints mentioned therein.</li>
   * </ul>
   */
  @Ignore('Only execute by hand to not overflow target with lots of dummy client registrations')
  def "Issuing Explicit Client Registration against ID operator 'id.test.denic.de'"() {
    given:
    def rpTestDenicDe = URI.create('https://rp.test.denic.de')
    def privPubJWKOfRPTestDenicDe = ECKey.parse('''{
          "kty": "EC",
          "d": "jnaV2sHq_PgGt6H50qJaq3jQPzWPdKY9NMRyZgEAQ_I",
          "use": "sig",
          "crv": "P-256",
          "kid": "DENIC-ID-Test-Shop",
          "x": "yoWKh2ygjSKJ2Np4paXJv69m1AA2yrT-UESg4ae5spk",
          "y": "C4IzWzg2WU8mn5WhGECeLVAMQLHwfSxcLUNCuQKr5q0"
    }''')
    def redirectURIOfRPTestDenicDe = URI.create('https://rp.test.denic.de/callback')
    def logoURIOfRPTestDenicDe = URI.create('https://rp.test.denic.de/logo.png')
    ZonedDateTime issuingTimestamp = ZonedDateTime.now()
    def nameOfRPTestDenicDe = "ExplicitClientRegistrationClientIntegrationTest_${issuingTimestamp.format(ofPattern('yyyyMMdd_HHmmSSX'))}"
    def metaDataOfRPTestDenicDe = new RelyingPartyMetaData.Impl(nameOfRPTestDenicDe,
            [redirectURIOfRPTestDenicDe],
            logoURIOfRPTestDenicDe)
    def authorityHints = [URI.create('https://id.test.denic.de/dummy/trust-anchor')]
    ExplicitClientRegistrationRequest request = new ExplicitClientRegistrationRequest.Impl(rpTestDenicDe,
            issuingTimestamp,
            issuingTimestamp.plus(7L, YEARS),
            new JWKSet(privPubJWKOfRPTestDenicDe),
            metaDataOfRPTestDenicDe,
            authorityHints)
    def jwsOfRequest = JWSOfSelfSigned.signedWithIts1stJWK(request)
    URI openIdProvider = URI.create('https://id.staging.denic.de')

    when:
    def registrationResponse = CUT.registerClientWith(openIdProvider)
            .atResolvedClientRegistrationEndpoint()
            .explicitly(jwsOfRequest)

    then:
    with(registrationResponse.getPayload()) {
      it.issuer == openIdProvider
      it.subject == rpTestDenicDe
      it.isValidNow()
      !it.isSelfSigned()
      println "- Authority Hints accepted by OpenID Provider '${openIdProvider}': ${it.authorityHints}"
      with(it.relyingPartyMetaDataPolicy) {
        println "- OpenID Provider '${openIdProvider}' assign to '${rpTestDenicDe}' the following ID: ${it.clientID}"
        println "- Secret assigned to this Relying Party: '${it.clientSecret}'"
        println "- This secret expires at: ${it.secretExpiresAt}"
      }
    }
  }

}
