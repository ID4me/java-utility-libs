/*^
  ===========================================================================
  ID4me Utility Libs
  ===========================================================================
  Copyright (C) 2020 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.util.wellknown.openidfederation;

import com.fasterxml.jackson.databind.ObjectMapper
import com.nimbusds.jose.jwk.JWK
import com.nimbusds.jose.jwk.JWKSet
import spock.lang.Specification

class ExplicitClientRegistrationRequestTest extends Specification {

  private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper()

  def 'JSON marshalling and unmarshalling of correct instance'() {
    given:
    def expectedJSON = '''{
        "iss"            : "http://issuer.uri",
        "sub"            : "http://issuer.uri",
        "iat"            : 1594030600,
        "exp"            : 1894635400,
        "jwks"           : { "keys": [ {
                                 "kty": "RSA",
                                 "e"  : "AQAB",
                                 "use": "sig",
                                 "kid": "exR5",
                                 "alg": "RS256",
                                 "n"  : "l9TeUfN0jztln5hVq6Z3vwS47MCyonpO-kJSVMqccKgoUkxLzo_IH1ekKf-3X1Tu4KrKoDn7Nk6Wrusw9gOI9JCszV8rCE1_SCYnKI4mCwI9RXhCgXC0NkvXg-1ySHn9PjNEurGsgpIFqA2u-66KItFP_BLsUKGDfC1w73EymUJ6ZHGc1FnAXCusWgLARceOep4oAO8q3_oFNW4A__1IphYnJ6zdqYwBHK6PWf210SKP8LAJ0tlq7RTZyiB0DG9ina95UHNFIoJnc_g-AOCa1-ShDcUNpWtpL1j3vZnAHyG3pB_9xi4Ngo2-vlZQXnalZmDbk1Cog4N3hI-3DXTTMw" } ] },
        "authority_hints": [ "http://authority.hint" ],
        "metadata"       : {
            "openid_relying_party": {
                "client_name"              : "Test-Client",
                "redirect_uris"            : [ "http://redirect.uri" ],
                "logo_uri"                 : "http://logo.uri",
                "client_registration_types": [ "explicit" ]
            }
        }
}'''
    RelyingPartyMetaData relyingPartyMetaData = new RelyingPartyMetaData.Impl('Test-Client',
            Set.of(URI.create('http://redirect.uri')),
            URI.create('http://logo.uri'))
    JWKSet jwkSet = new JWKSet(JWK.parse('''{
        "kty": "RSA",
        "e"  : "AQAB",
        "use": "sig",
        "kid": "exR5",
        "alg": "RS256",
        "n"  : "l9TeUfN0jztln5hVq6Z3vwS47MCyonpO-kJSVMqccKgoUkxLzo_IH1ekKf-3X1Tu4KrKoDn7Nk6Wrusw9gOI9JCszV8rCE1_SCYnKI4mCwI9RXhCgXC0NkvXg-1ySHn9PjNEurGsgpIFqA2u-66KItFP_BLsUKGDfC1w73EymUJ6ZHGc1FnAXCusWgLARceOep4oAO8q3_oFNW4A__1IphYnJ6zdqYwBHK6PWf210SKP8LAJ0tlq7RTZyiB0DG9ina95UHNFIoJnc_g-AOCa1-ShDcUNpWtpL1j3vZnAHyG3pB_9xi4Ngo2-vlZQXnalZmDbk1Cog4N3hI-3DXTTMw" }
'''))
    ExplicitClientRegistrationRequest CUT = new ExplicitClientRegistrationRequest.Impl(
            URI.create('http://issuer.uri'),
            1594030600L, 1894635400L,
            jwkSet,
            relyingPartyMetaData,
            Set.of(URI.create('http://authority.hint'))
    )

    when:
    String marshalledCUT = OBJECT_MAPPER.writerFor(ExplicitClientRegistrationRequest).writeValueAsString(CUT)

    then:
    marshalledCUT == expectedJSON.replaceAll('(\\h|\\v)', '');

    when:
    println "Going to unmarshall '$marshalledCUT'"
    def unmarshalledCUT = OBJECT_MAPPER.readerFor(ExplicitClientRegistrationRequest).readValue(marshalledCUT)

    then:
    unmarshalledCUT.equals(CUT)
  }

}
