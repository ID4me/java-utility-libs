/*^
  ===========================================================================
  ID4me Utility Libs
  ===========================================================================
  Copyright (C) 2020 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.util.wellknown.openidfederation;

import de.denic.util.jws.JWKSetSupplier;
import de.denic.util.jws.JWSOfSelfSigned;
import de.denic.util.jws.JWSParsingException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.annotation.concurrent.Immutable;
import java.net.URI;
import java.util.Optional;
import java.util.function.BiFunction;

import static java.util.Optional.empty;
import static java.util.Optional.of;

public interface HTTPResponseBodyToSelfSignedJWSConverter<EXCEPTION extends HTTPResponseBodyToJWSConverter.ConverterException>
        extends HTTPResponseBodyToJWSConverter<EXCEPTION> {

  @Nonnull
  <RESULT extends JWKSetSupplier> JWSOfSelfSigned<RESULT> asJWSOfSelfSigned(@Nonnull Class<? extends RESULT> resultType) throws EXCEPTION;

  @Nonnull
  <RESULT extends JWKSetSupplier> Optional<JWSOfSelfSigned<RESULT>> asOptJWSOfSelfSigned(@Nonnull Class<? extends RESULT> resultType);

  @Immutable
  abstract class Base<EX extends ConverterException> extends HTTPResponseBodyToJWSConverter.Base<EX> implements HTTPResponseBodyToSelfSignedJWSConverter<EX> {

    private static final Logger LOG = LoggerFactory.getLogger(HTTPResponseBodyToSelfSignedJWSConverter.Base.class);

    protected Base(@Nonnull final String response, @Nonnull final URI targetURI,
                   @Nonnull final String operationDescriptionForLogging,
                   @Nonnull final BiFunction<Exception, Class<?>, EX> parsingExceptionProvider) {
      super(response, targetURI, operationDescriptionForLogging, parsingExceptionProvider);
    }

    @Nonnull
    @Override
    public final <RESULT extends JWKSetSupplier> JWSOfSelfSigned<RESULT> asJWSOfSelfSigned(@Nonnull final Class<? extends RESULT> resultType) throws EX {
      try {
        final JWSOfSelfSigned<RESULT> result = JWSOfSelfSigned.from(getResponse(), resultType);
        LOG.info("{} of '{}': {}", getOperationDescriptionForLogging(), getTargetURI(), result);
        return result;
      } catch (final JWSParsingException e) {
        throw getParsingExceptionProvider().apply(e, resultType);
      }
    }

    @Nonnull
    @Override
    public final <RESULT extends JWKSetSupplier> Optional<JWSOfSelfSigned<RESULT>> asOptJWSOfSelfSigned(@Nonnull final Class<? extends RESULT> resultType) {
      try {
        return of(asJWSOfSelfSigned(resultType));
      } catch (final ConverterException e) {
        LOG.warn(e.getMessage(), e);
        return empty();
      }
    }

  }

}