/*^
  ===========================================================================
  ID4me Utility Libs
  ===========================================================================
  Copyright (C) 2020 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.util.wellknown.openidfederation.impl;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Optional;

import static java.time.Instant.now;
import static java.time.Instant.ofEpochSecond;
import static java.util.Objects.requireNonNull;
import static java.util.Optional.ofNullable;

public interface ValidityConstrained {

  ZoneId UTC = ZoneId.of("UTC");

  @JsonGetter("iat")
  default long getIAT() {
    return getIssuedAt().toEpochSecond();
  }

  @JsonGetter("exp")
  default long getEXP() {
    return getExpiresAt().map(ZonedDateTime::toEpochSecond).orElse(0L);
  }

  @JsonIgnore
  @Nonnull
  ZonedDateTime getIssuedAt();

  /**
   * @return {@link Optional#empty()} if instance does <strong>NOT</strong> expire (in this case {@link #getEXP()} returns <code>0</code>)!
   */
  @JsonIgnore
  @Nonnull
  Optional<ZonedDateTime> getExpiresAt();

  @JsonIgnore
  default boolean isValidNow() {
    return isValidAt(now());
  }

  default boolean isValidAt(@Nonnull final Instant timestamp) {
    return isValidAt(timestamp.atZone(UTC));
  }

  default boolean isValidAt(@Nonnull final ZonedDateTime timestamp) {
    final ZonedDateTime issuedAt = getIssuedAt();
    return (issuedAt.isBefore(requireNonNull(timestamp, "Missing timestamp")) || issuedAt.equals(timestamp))
            && getExpiresAt().map(expires -> expires.isAfter(timestamp)).orElse(true);
  }

  @Nonnull
  static ZonedDateTime fromUTCEpochSeconds(@Nonnull final Long epochSecond,
                                           @Nonnull final String errorMessageIfMissing) {
    return ofEpochSecond(requireNonNull(epochSecond, errorMessageIfMissing)).atZone(UTC);
  }

  /**
   * @return <code>null</code> with input value <code>0</code>!
   */
  @Nullable
  static ZonedDateTime optFromUTCEpochSeconds(@Nonnull final Long epochSecond,
                                              @Nonnull final String errorMessageIfMissing) {
    return (requireNonNull(epochSecond, errorMessageIfMissing) == 0L ? null : ofEpochSecond(epochSecond).atZone(UTC));
  }

  @Immutable
  class Impl implements ValidityConstrained {

    private final ZonedDateTime issuedAt, expiresAt;

    public Impl(@Nonnull final ZonedDateTime issuedAt,
                @Nullable final ZonedDateTime expiresAt) {
      this.issuedAt = requireNonNull(issuedAt, "Missing issuing timestamp (JSON key 'iat')");
      this.expiresAt = expiresAt;
    }

    @Nonnull
    @Override
    public ZonedDateTime getIssuedAt() {
      return issuedAt;
    }

    @Nonnull
    @Override
    public Optional<ZonedDateTime> getExpiresAt() {
      return ofNullable(expiresAt);
    }

    @Override
    public boolean equals(final Object o) {
      if (this == o)
        return true;
      if (o == null || getClass() != o.getClass())
        return false;

      final Impl impl = (Impl) o;

      if (!issuedAt.equals(impl.issuedAt))
        return false;
      return expiresAt != null ? expiresAt.equals(impl.expiresAt) : impl.expiresAt == null;
    }

    @Override
    public int hashCode() {
      int result = issuedAt.hashCode();
      result = 31 * result + (expiresAt != null ? expiresAt.hashCode() : 0);
      return result;
    }

    @Override
    public String toString() {
      return "valid=[" + issuedAt +
              " - " + (expiresAt == null ? "FOREVER" : expiresAt) + ']';
    }

  }

}
