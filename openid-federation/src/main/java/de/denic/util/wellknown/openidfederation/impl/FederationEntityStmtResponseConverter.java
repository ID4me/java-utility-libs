/*^
  ===========================================================================
  ID4me Utility Libs
  ===========================================================================
  Copyright (C) 2020 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.util.wellknown.openidfederation.impl;

import de.denic.util.wellknown.openidfederation.FederationEntityConfig;
import de.denic.util.wellknown.openidfederation.FederationEntityStmtClient;
import de.denic.util.wellknown.openidfederation.FederationEntityStmtParsingException;
import de.denic.util.wellknown.openidfederation.HTTPResponseBodyToJWSConverter;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;
import java.net.URI;
import java.util.Optional;

import static java.util.Optional.ofNullable;

@Immutable
public final class FederationEntityStmtResponseConverter extends HTTPResponseBodyToJWSConverter.Base<FederationEntityStmtParsingException>
        implements FederationEntityStmtClient.ResponseConverter {

  private final FederationEntityConfig federationEntityConfigOfIssuer;

  public FederationEntityStmtResponseConverter(@Nonnull final URI issuer,
                                               @Nullable final FederationEntityConfig federationEntityConfigOfIssuer,
                                               @Nonnull final String responseBody,
                                               @Nonnull final URI subject) {
    super(responseBody, issuer, "Unmarshalling Federation Entity Statement JWS",
            (exception, targetType) -> new FederationEntityStmtParsingException("Unmarshalling Federation Entity Statement JWS issued by '" + issuer + "' regarding subject '" + subject + "' to type '" + targetType.getName() + "' failed.", exception));
    this.federationEntityConfigOfIssuer = federationEntityConfigOfIssuer;
  }

  @Nonnull
  @Override
  public Optional<FederationEntityConfig> optFederationEntityConfigOfIssuer() {
    return ofNullable(federationEntityConfigOfIssuer);
  }

}
