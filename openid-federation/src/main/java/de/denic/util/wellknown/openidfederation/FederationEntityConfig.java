/*^
  ===========================================================================
  ID4me Utility Libs
  ===========================================================================
  Copyright (C) 2020 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.util.wellknown.openidfederation;

import com.fasterxml.jackson.annotation.*;
import com.fasterxml.jackson.core.JsonPointer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.nimbusds.jose.jwk.JWKSet;
import de.denic.util.wellknown.MissingPropertyException;
import de.denic.util.wellknown.openidfederation.impl.CommonFederationEntityStmt;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;
import java.net.URI;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_NULL;
import static de.denic.util.wellknown.openidfederation.impl.RequirementUtil.requireEqual;
import static java.util.Collections.emptySet;
import static java.util.Objects.requireNonNullElse;
import static java.util.Optional.ofNullable;
import static java.util.stream.Collectors.toUnmodifiableSet;

@JsonSerialize(as = FederationEntityConfig.Impl.class)
@JsonDeserialize(as = FederationEntityConfig.Impl.class)
public interface FederationEntityConfig extends CommonFederationEntityStmt {

  /**
   * @return Usually immutable!
   */
  @JsonGetter("authority_hints")
  @Nonnull
  Set<URI> getAuthorityHints();

  /**
   * If no Authority Hints are given (see <a href="https://openid.net/specs/openid-connect-federation-1_0.html#entity-statement">chapter 2.1 of specification</a>).
   *
   * @see #getAuthorityHints()
   */
  @JsonIgnore
  default boolean isTrustAnchor() {
    return getAuthorityHints().isEmpty();
  }

  @JsonIgnore
  @Nonnull
  Optional<URI> getOptFederationApiEndpoint();

  @JsonIgnore
  @Nonnull
  URI getFederationApiEndpoint() throws MissingPropertyException;

  @JsonIgnore
  @Nonnull
  Optional<Integer> getMaxPathLength();

  @Immutable
  @JsonIgnoreProperties(ignoreUnknown = true)
  final class Impl extends CommonFederationEntityStmt.Impl implements FederationEntityConfig {

    public static final Collection<URI> NO_AUTHORITY_HINTS = emptySet();

    private static final JsonPointer FEDERATION_API_ENDPOINT_JSON_PATH = JsonPointer.compile("/federation_entity/federation_api_endpoint");
    private static final JsonNodeFactory JSON_NODE_FACTORY = JsonNodeFactory.instance;

    private final URI optFederationApiEndpoint;
    private final Set<URI> fixedAuthorityHints;
    private final Integer optMaxPathLength;

    public Impl(@Nonnull final URI issuer,
                @Nonnull final Long issuedAt,
                @Nonnull final Long expiresAt,
                @Nullable final JWKSet jwkSetOfSubject,
                @Nullable final JsonNode metaDataJsonNode,
                @Nullable final JsonNode constraintsJsonNode,
                @Nullable final Collection<URI> authorityHints) {
      super(issuer, issuer, issuedAt, expiresAt, jwkSetOfSubject);
      this.optFederationApiEndpoint = (metaDataJsonNode == null ? null : extractFederationApiEndpointFrom(metaDataJsonNode));
      this.optMaxPathLength = (constraintsJsonNode == null ? null : extractMaxPathLengthFrom(constraintsJsonNode));
      this.fixedAuthorityHints = requireNonNullElse(authorityHints, NO_AUTHORITY_HINTS).stream()
              .collect(toUnmodifiableSet());
    }

    public Impl(@Nonnull @JsonProperty("iss") final URI issuer,
                @Nonnull @JsonProperty("sub") final URI subject,
                @Nonnull @JsonProperty("iat") final Long issuedAt,
                @Nonnull @JsonProperty("exp") final Long expiresAt,
                @Nonnull @JsonProperty("jwks") final JsonNode jwksJsonValue,
                @Nullable @JsonProperty("metadata") final JsonNode metaDataJsonNode,
                @Nullable @JsonProperty("constraints") final JsonNode constraintsJsonNode,
                @Nullable @JsonProperty("authority_hints") final Collection<URI> authorityHints) {
      this(requireEqual(issuer, subject),
              issuedAt, expiresAt,
              parseToJWKSet(jwksJsonValue),
              metaDataJsonNode, constraintsJsonNode,
              authorityHints);
    }

    @Nullable
    private Integer extractMaxPathLengthFrom(final JsonNode constraintsJsonNode) {
      final JsonNode maxPathLengthJsonNode = constraintsJsonNode.get("max_path_length");
      return (maxPathLengthJsonNode == null ? null : maxPathLengthJsonNode.asInt());
    }

    @Nullable
    private URI extractFederationApiEndpointFrom(@Nonnull final JsonNode metaDataJsonNode) {
      final JsonNode federationApiEndpointJsonNode = metaDataJsonNode.at(FEDERATION_API_ENDPOINT_JSON_PATH);
      return (federationApiEndpointJsonNode.isMissingNode() ? null : URI.create(federationApiEndpointJsonNode.asText()));
    }

    @Nonnull
    @Override
    public Optional<Integer> getMaxPathLength() {
      return ofNullable(optMaxPathLength);
    }

    @Nonnull
    @Override
    public Optional<URI> getOptFederationApiEndpoint() {
      return ofNullable(optFederationApiEndpoint);
    }

    @Nonnull
    @Override
    public URI getFederationApiEndpoint() throws MissingPropertyException {
      if (optFederationApiEndpoint == null) {
        throw new MissingPropertyException("Federation Entity Configuration", getIssuer(),
                "Federation API Endpoint URI", FEDERATION_API_ENDPOINT_JSON_PATH.toString());
      }

      return optFederationApiEndpoint;
    }

    @JsonInclude(NON_NULL)
    @JsonGetter("metadata")
    @Nullable
    public ObjectNode getMetaDataObjectNode() {
      if (optFederationApiEndpoint == null) {
        return null;
      }

      final ObjectNode metaDataNode = JSON_NODE_FACTORY.objectNode();
      metaDataNode.putObject("federation_entity")
              .put("federation_api_endpoint", optFederationApiEndpoint.toASCIIString());
      return metaDataNode;
    }

    @Nonnull
    @Override
    public Set<URI> getAuthorityHints() {
      return fixedAuthorityHints;
    }

    @Override
    public boolean equals(final Object o) {
      if (this == o)
        return true;
      if (o == null || getClass() != o.getClass())
        return false;
      if (!super.equals(o))
        return false;

      final FederationEntityConfig.Impl impl = (FederationEntityConfig.Impl) o;

      if (optFederationApiEndpoint != null ? !optFederationApiEndpoint.equals(impl.optFederationApiEndpoint) : impl.optFederationApiEndpoint != null)
        return false;
      if (!fixedAuthorityHints.equals(impl.fixedAuthorityHints))
        return false;
      return optMaxPathLength != null ? optMaxPathLength.equals(impl.optMaxPathLength) : impl.optMaxPathLength == null;
    }

    @Override
    public int hashCode() {
      int result = super.hashCode();
      result = 31 * result + (optFederationApiEndpoint != null ? optFederationApiEndpoint.hashCode() : 0);
      result = 31 * result + fixedAuthorityHints.hashCode();
      result = 31 * result + (optMaxPathLength != null ? optMaxPathLength.hashCode() : 0);
      return result;
    }

    @Override
    public String toString() {
      return "{" + super.toString() +
              ", federationApiEndpoint=" + (optFederationApiEndpoint == null ? "<NULL>" : "'" + optFederationApiEndpoint + "'") +
              ", authorityHints=" + fixedAuthorityHints +
              "', maxPathLength=" + (Objects.toString(optMaxPathLength, "<NULL>")) +
              '}';
    }

  }

}
