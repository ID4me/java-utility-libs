/*^
  ===========================================================================
  ID4me Utility Libs
  ===========================================================================
  Copyright (C) 2020 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.util.wellknown.openidfederation;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;
import java.time.ZonedDateTime;
import java.util.Optional;

import static de.denic.util.wellknown.openidfederation.impl.ValidityConstrained.fromUTCEpochSeconds;
import static de.denic.util.wellknown.openidfederation.impl.ValidityConstrained.optFromUTCEpochSeconds;
import static java.util.Objects.requireNonNull;
import static java.util.Optional.ofNullable;

@JsonDeserialize(as = RelyingPartyMetaDataPolicy.Impl.class)
public interface RelyingPartyMetaDataPolicy {

  @Nonnull
  String getClientID();

  @Nonnull
  ZonedDateTime getIdIssuedAt();

  @Nonnull
  String getClientSecret();

  @Nonnull
  Optional<ZonedDateTime> getSecretExpiresAt();

  @Immutable
  @JsonIgnoreProperties(ignoreUnknown = true)
  final class Impl implements RelyingPartyMetaDataPolicy {

    private final String clientID;
    private final ZonedDateTime clientIDIssuedAt;
    private final String clientSecret;
    private final ZonedDateTime clientSecretExpiresAt;

    public Impl(@Nonnull final String clientID,
                @Nonnull final ZonedDateTime clientIDIssuedAt,
                @Nonnull final String clientSecret,
                @Nullable final ZonedDateTime clientSecretExpiresAt) {
      this.clientID = requireNonNull(clientID, "Missing Client ID (JSON key 'client_id')");
      this.clientIDIssuedAt = requireNonNull(clientIDIssuedAt, "Missing Client ID issuing timestamp (JSON key 'client_id_issued_at')");
      this.clientSecret = requireNonNull(clientSecret, "Missing Client's Secret (JSON key 'client_secret')");
      this.clientSecretExpiresAt = clientSecretExpiresAt;
    }

    public Impl(@JsonProperty("client_id") @Nonnull final JsonNode clientIDJsonNode,
                @JsonProperty("client_id_issued_at") @Nonnull final JsonNode clientIDIssuedAtJsonNode,
                @JsonProperty("client_secret") @Nonnull final JsonNode clientSecretJsonNode,
                @JsonProperty("client_secret_expires_at") @Nonnull final JsonNode clientSecretExpiresAtJsonNode) {
      this(stringValueFrom(clientIDJsonNode),
              timestampValueFrom(clientIDIssuedAtJsonNode, "Missing Client ID issuing timestamp (JSON key 'client_id_issued_at')"),
              stringValueFrom(clientSecretJsonNode),
              optTimestampValueFrom(clientSecretExpiresAtJsonNode, "Missing Client's Secret expiring timestamp (JSON key 'client_secret_expires_at')"));
    }

    @Nonnull
    private static ZonedDateTime timestampValueFrom(@Nonnull final JsonNode jsonNode,
                                                    @Nonnull final String errorMessageIfMissing) {
      final JsonNode valueNode = requireNonNull(jsonNode, errorMessageIfMissing).get("value");
      if (!valueNode.isNumber()) {
        throw new IllegalArgumentException("Not a numeric type. " + errorMessageIfMissing);
      }

      return fromUTCEpochSeconds(valueNode.longValue(), errorMessageIfMissing);
    }

    @Nullable
    private static ZonedDateTime optTimestampValueFrom(@Nonnull final JsonNode jsonNode,
                                                       @Nonnull final String errorMessageIfMissing) {
      final JsonNode valueNode = requireNonNull(jsonNode, errorMessageIfMissing).get("value");
      if (!valueNode.isNumber()) {
        throw new IllegalArgumentException("Not a numeric type. " + errorMessageIfMissing);
      }

      return optFromUTCEpochSeconds(valueNode.longValue(), errorMessageIfMissing);
    }

    @Nonnull
    private static String stringValueFrom(@Nonnull final JsonNode jsonNode) {
      return jsonNode.get("value").textValue();
    }

    @Nonnull
    @Override
    public String getClientID() {
      return clientID;
    }

    @Nonnull
    @Override
    public ZonedDateTime getIdIssuedAt() {
      return clientIDIssuedAt;
    }

    @Nonnull
    @Override
    public String getClientSecret() {
      return clientSecret;
    }

    @Nonnull
    @Override
    public Optional<ZonedDateTime> getSecretExpiresAt() {
      return ofNullable(clientSecretExpiresAt);
    }

    @Override
    public boolean equals(final Object o) {
      if (this == o)
        return true;
      if (o == null || getClass() != o.getClass())
        return false;

      final Impl impl = (Impl) o;

      if (!clientID.equals(impl.clientID))
        return false;
      if (!clientIDIssuedAt.equals(impl.clientIDIssuedAt))
        return false;
      if (!clientSecret.equals(impl.clientSecret))
        return false;
      return clientSecretExpiresAt != null ? clientSecretExpiresAt.equals(impl.clientSecretExpiresAt) : impl.clientSecretExpiresAt == null;
    }

    @Override
    public int hashCode() {
      int result = clientID.hashCode();
      result = 31 * result + clientIDIssuedAt.hashCode();
      result = 31 * result + clientSecret.hashCode();
      result = 31 * result + (clientSecretExpiresAt != null ? clientSecretExpiresAt.hashCode() : 0);
      return result;
    }

    @Override
    public String toString() {
      return "{clientID='" + clientID + '\'' +
              ", issuedAt=" + clientIDIssuedAt +
              ", clientSecret=[" + clientSecret.length() + " chars]" +
              ", secretExpiresAt=" + (clientSecretExpiresAt == null ? "<NEVER>" : clientSecretExpiresAt) +
              '}';
    }

  }

}
