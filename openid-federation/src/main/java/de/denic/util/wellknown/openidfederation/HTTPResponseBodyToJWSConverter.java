/*^
  ===========================================================================
  ID4me Utility Libs
  ===========================================================================
  Copyright (C) 2020 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.util.wellknown.openidfederation;

import com.fasterxml.jackson.databind.JsonNode;
import de.denic.util.jws.JWSOf;
import de.denic.util.jws.JWSParsingException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.annotation.concurrent.Immutable;
import javax.annotation.concurrent.NotThreadSafe;
import java.net.URI;
import java.util.Optional;
import java.util.function.BiFunction;

import static java.util.Objects.requireNonNull;
import static java.util.Optional.empty;
import static java.util.Optional.of;

public interface HTTPResponseBodyToJWSConverter<EXCEPTION extends HTTPResponseBodyToJWSConverter.ConverterException> {

  @Nonnull
  JsonNode asJsonNodeOfPayload() throws EXCEPTION;

  @Nonnull
  Optional<JsonNode> asOptJsonNodeOfPayload();

  @Nonnull
  <RESULT> JWSOf<RESULT> asJWSOf(@Nonnull Class<? extends RESULT> resultType) throws EXCEPTION;

  @Nonnull
  <RESULT> Optional<JWSOf<RESULT>> asOptJWSOf(@Nonnull Class<? extends RESULT> resultType);

  @Immutable
  abstract class Base<EX extends ConverterException> implements HTTPResponseBodyToJWSConverter<EX> {

    private static final Logger LOG = LoggerFactory.getLogger(Base.class);

    private final String response;
    private final URI targetURI;
    private final String operationDescriptionForLogging;
    private final BiFunction<Exception, Class<?>, EX> parsingExceptionProvider;

    protected Base(@Nonnull final String response, @Nonnull final URI targetURI,
                   @Nonnull final String operationDescriptionForLogging,
                   @Nonnull final BiFunction<Exception, Class<?>, EX> parsingExceptionProvider) {
      this.response = requireNonNull(response, "Missing response");
      this.targetURI = requireNonNull(targetURI, "Missing target URI");
      this.operationDescriptionForLogging = requireNonNull(operationDescriptionForLogging, "Missing operation description");
      this.parsingExceptionProvider = requireNonNull(parsingExceptionProvider, "Missing exception provider");
    }

    @Nonnull
    @Override
    public final JsonNode asJsonNodeOfPayload() throws EX {
      return asJWSOf(JsonNode.class).getPayload();
    }

    @Nonnull
    @Override
    public final Optional<JsonNode> asOptJsonNodeOfPayload() {
      return asOptJWSOf(JsonNode.class).map(JWSOf::getPayload);
    }

    @Nonnull
    @Override
    public final <RESULT> JWSOf<RESULT> asJWSOf(@Nonnull final Class<? extends RESULT> resultType) throws EX {
      try {
        final JWSOf<RESULT> result = JWSOf.from(response, resultType);
        LOG.info("{} of '{}': {}", operationDescriptionForLogging, targetURI, result);
        return result;
      } catch (final JWSParsingException e) {
        throw parsingExceptionProvider.apply(e, resultType);
      }
    }

    @Nonnull
    @Override
    public final <RESULT> Optional<JWSOf<RESULT>> asOptJWSOf(@Nonnull final Class<? extends RESULT> resultType) {
      try {
        return of(asJWSOf(resultType));
      } catch (final ConverterException e) {
        LOG.warn(e.getMessage(), e);
        return empty();
      }
    }

    @Nonnull
    protected final String getResponse() {
      return response;
    }

    @Nonnull
    protected final URI getTargetURI() {
      return targetURI;
    }

    @Nonnull
    protected final String getOperationDescriptionForLogging() {
      return operationDescriptionForLogging;
    }

    @Nonnull
    protected final BiFunction<Exception, Class<?>, EX> getParsingExceptionProvider() {
      return parsingExceptionProvider;
    }

  }

  @NotThreadSafe
  abstract class ConverterException extends RuntimeException {

    private static final long serialVersionUID = -2999658166917270589L;

    protected ConverterException(final String message, final Throwable cause) {
      super(message, cause);
    }

  }

}