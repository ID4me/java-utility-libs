/*^
  ===========================================================================
  ID4me Utility Libs
  ===========================================================================
  Copyright (C) 2020 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.util.wellknown.openidfederation;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.nimbusds.jose.jwk.JWKSet;
import de.denic.util.wellknown.openidfederation.impl.CommonFederationEntityStmt;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;
import java.net.URI;

@JsonSerialize(as = FederationEntityStmt.Impl.class)
@JsonDeserialize(as = FederationEntityStmt.Impl.class)
public interface FederationEntityStmt extends CommonFederationEntityStmt {

  // Intentionally left empty at the moment.

  @Immutable
  @JsonIgnoreProperties(ignoreUnknown = true)
  final class Impl extends CommonFederationEntityStmt.Impl implements FederationEntityStmt {

    public Impl(@Nonnull final URI issuer,
                @Nonnull final URI subject,
                @Nonnull final Long issuedAt,
                @Nonnull final Long expiresAt,
                @Nullable final JWKSet jwkSetOfSubject) {
      super(issuer, subject, issuedAt, expiresAt, jwkSetOfSubject);
    }

    public Impl(@Nonnull @JsonProperty("iss") final URI issuer,
                @Nonnull @JsonProperty("sub") final URI subject,
                @Nonnull @JsonProperty("iat") final Long issuedAt,
                @Nonnull @JsonProperty("exp") final Long expiresAt,
                @Nonnull @JsonProperty("jwks") final JsonNode jwksJsonValue) {
      this(issuer, subject,
              issuedAt, expiresAt,
              parseToJWKSet(jwksJsonValue));
    }

    @Override
    public String toString() {
      return "{" + super.toString() +
              '}';
    }

  }

}
