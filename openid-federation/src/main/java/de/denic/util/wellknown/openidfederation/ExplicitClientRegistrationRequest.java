/*^
  ===========================================================================
  ID4me Utility Libs
  ===========================================================================
  Copyright (C) 2020 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.util.wellknown.openidfederation;

import com.fasterxml.jackson.annotation.*;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.nimbusds.jose.jwk.JWKSet;
import de.denic.util.wellknown.openidfederation.impl.CommonFederationEntityStmt;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;
import java.io.IOException;
import java.net.URI;
import java.time.ZonedDateTime;
import java.util.Collection;
import java.util.Objects;
import java.util.Set;

import static de.denic.util.wellknown.openidfederation.impl.RequirementUtil.requireEqual;
import static de.denic.util.wellknown.openidfederation.impl.ValidityConstrained.fromUTCEpochSeconds;
import static java.util.Collections.emptySet;
import static java.util.Objects.requireNonNull;
import static java.util.Objects.requireNonNullElse;
import static java.util.stream.Collectors.toUnmodifiableSet;

@JsonSerialize(as = ExplicitClientRegistrationRequest.Impl.class)
@JsonDeserialize(as = ExplicitClientRegistrationRequest.Impl.class)
public interface ExplicitClientRegistrationRequest extends CommonFederationEntityStmt {

  /**
   * @return Usually immutable!
   */
  @Nonnull
  Set<URI> getAuthorityHints();

  @Nonnull
  RelyingPartyMetaData getRelyingPartyMetaData();

  @Immutable
  @JsonIgnoreProperties(ignoreUnknown = true)
  @JsonPropertyOrder({"iss", "sub", "iat", "exp", "jwks", "authority_hints", "metadata"})
  final class Impl extends CommonFederationEntityStmt.Impl implements ExplicitClientRegistrationRequest {

    private static final Collection<URI> NO_AUTHORITY_HINTS = emptySet();
    private static final JsonNodeFactory JSON_NODE_FACTORY = JsonNodeFactory.instance;
    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper()
            .registerModule(new Jdk8Module().configureAbsentsAsNulls(true));

    private final Set<URI> fixedAuthorityHints;
    private final RelyingPartyMetaData relyingPartyMetaData;

    public Impl(@Nonnull final URI issuer,
                @Nonnull final ZonedDateTime issuedAt,
                @Nonnull final ZonedDateTime expiresAt,
                @Nullable final JWKSet jwkSet,
                @Nonnull final RelyingPartyMetaData relyingPartyMetaData,
                @Nullable final Collection<URI> authorityHints) {
      super(issuer, issuer, issuedAt, expiresAt, jwkSet);
      this.relyingPartyMetaData = requireNonNull(relyingPartyMetaData, "Missing relying party meta-data");
      this.fixedAuthorityHints = requireNonNullElse(authorityHints, NO_AUTHORITY_HINTS).stream()
              .filter(Objects::nonNull)
              .collect(toUnmodifiableSet());
    }

    public Impl(@Nonnull final URI issuer,
                @Nonnull final Long issuedAt,
                @Nonnull final Long expiresAt,
                @Nullable final JWKSet jwkSet,
                @Nonnull final RelyingPartyMetaData relyingPartyMetaData,
                @Nullable final Collection<URI> authorityHints) {
      super(issuer, issuer,
              fromUTCEpochSeconds(issuedAt, "Missing issuing timestamp (JSON key 'iat')"),
              fromUTCEpochSeconds(expiresAt, "Missing expiration timestamp (JSON key 'exp')"),
              jwkSet);
      this.relyingPartyMetaData = requireNonNull(relyingPartyMetaData, "Missing relying party meta-data");
      this.fixedAuthorityHints = requireNonNullElse(authorityHints, NO_AUTHORITY_HINTS).stream()
              .filter(Objects::nonNull)
              .collect(toUnmodifiableSet());
    }

    public Impl(@JsonProperty("iss") @Nonnull final URI issuer,
                @JsonProperty("sub") @Nonnull final URI subject,
                @JsonProperty("iat") @Nonnull final Long issuedAt,
                @JsonProperty("exp") @Nonnull final Long expiresAt,
                @JsonProperty("jwks") @Nullable final JsonNode jwksJsonNode,
                @JsonProperty("metadata") @Nonnull final JsonNode metaDataJsonNode,
                @JsonProperty("authority_hints") @Nullable final Collection<URI> authorityHints) {
      this(requireEqual(issuer, subject),
              issuedAt, expiresAt,
              (jwksJsonNode == null ? null : parseToJWKSet(jwksJsonNode)),
              parseToRelyingPartyMetaData(metaDataJsonNode),
              authorityHints);
    }

    @Nonnull
    private static RelyingPartyMetaData parseToRelyingPartyMetaData(@Nonnull final JsonNode metaDataJsonNode) {
      final JsonNode openIdRelyingPartyJsonNode = requireNonNull(metaDataJsonNode, "Missing JSON node 'metadata'")
              .get("openid_relying_party");
      try {
        return OBJECT_MAPPER.readerFor(RelyingPartyMetaData.class)
                .readValue(openIdRelyingPartyJsonNode);
      } catch (final IOException e) {
        throw new IllegalArgumentException(e);
      }
    }

    @JsonGetter("metadata")
    @Nonnull
    public ObjectNode getMetaDataObjectNode() {
      final ObjectNode metaDataNode = JSON_NODE_FACTORY.objectNode();
      metaDataNode.set("openid_relying_party", OBJECT_MAPPER.valueToTree(relyingPartyMetaData));
      return metaDataNode;
    }

    @JsonIgnore
    @Override
    @Nonnull
    public RelyingPartyMetaData getRelyingPartyMetaData() {
      return relyingPartyMetaData;
    }

    @JsonGetter("authority_hints")
    @Nonnull
    @Override
    public Set<URI> getAuthorityHints() {
      return fixedAuthorityHints;
    }

    @Override
    public boolean equals(final Object o) {
      if (this == o)
        return true;
      if (o == null || getClass() != o.getClass())
        return false;
      if (!super.equals(o))
        return false;

      final ExplicitClientRegistrationRequest.Impl impl = (ExplicitClientRegistrationRequest.Impl) o;

      if (!fixedAuthorityHints.equals(impl.fixedAuthorityHints))
        return false;
      return relyingPartyMetaData.equals(impl.relyingPartyMetaData);
    }

    @Override
    public int hashCode() {
      int result = super.hashCode();
      result = 31 * result + fixedAuthorityHints.hashCode();
      result = 31 * result + relyingPartyMetaData.hashCode();
      return result;
    }

    @Override
    public String toString() {
      return "{" + super.toString() +
              ", relyingPartyMetaData=" + relyingPartyMetaData +
              ", authorityHints=" + fixedAuthorityHints +
              '}';
    }

  }

}
