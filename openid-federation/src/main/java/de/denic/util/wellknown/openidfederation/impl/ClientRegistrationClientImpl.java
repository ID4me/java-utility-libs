/*^
  ===========================================================================
  ID4me Utility Libs
  ===========================================================================
  Copyright (C) 2020 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.util.wellknown.openidfederation.impl;

import de.denic.util.http.HTTPClient;
import de.denic.util.http.HTTPException;
import de.denic.util.http.HTTPResponseException;
import de.denic.util.jws.JWSOf;
import de.denic.util.jws.JWSOfSelfSigned;
import de.denic.util.wellknown.MissingPropertyException;
import de.denic.util.wellknown.openidconfig.OpenIDConfig;
import de.denic.util.wellknown.openidconfig.OpenIDConfigClient;
import de.denic.util.wellknown.openidconfig.impl.OpenIDConfigClientImpl;
import de.denic.util.wellknown.openidfederation.ClientRegistrationClient;
import de.denic.util.wellknown.openidfederation.ExplicitClientRegistrationRequest;
import de.denic.util.wellknown.openidfederation.ExplicitClientRegistrationResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.annotation.concurrent.Immutable;
import javax.annotation.concurrent.NotThreadSafe;
import java.net.URI;

import static de.denic.util.http.HTTPClient.shortenedBody;
import static java.util.Objects.requireNonNull;

@Immutable
public final class ClientRegistrationClientImpl implements ClientRegistrationClient {

  private static final Logger LOG = LoggerFactory.getLogger(ClientRegistrationClientImpl.class);

  private final HTTPClient httpClient;
  private final OpenIDConfigClient openIDConfigClient;

  public ClientRegistrationClientImpl(@Nonnull final HTTPClient httpClient,
                                      @Nonnull final OpenIDConfigClient openIDConfigClient) {
    this.httpClient = requireNonNull(httpClient, "Missing HTTP client");
    this.openIDConfigClient = requireNonNull(openIDConfigClient, "Missing OpenID Config client");
  }

  /**
   * Applies instance of {@link OpenIDConfigClientImpl} with {@link #ClientRegistrationClientImpl(HTTPClient, OpenIDConfigClient)}.
   */
  public ClientRegistrationClientImpl(@Nonnull final HTTPClient httpClient) {
    this(httpClient, new OpenIDConfigClientImpl(httpClient));
  }

  @Nonnull
  @Override
  public ClientRegistrationEndpointRequester registerClientWith(@Nonnull final URI openIdProvider) {
    return new BuilderImpl(openIdProvider);
  }

  @NotThreadSafe
  private final class BuilderImpl implements ClientRegistrationEndpointRequester, RegistrationRequestBuilder {

    private final URI openIdProvider;
    private URI clientRegistrationEndpointURI;

    private BuilderImpl(@Nonnull final URI openIdProvider) {
      this.openIdProvider = requireNonNull(openIdProvider, "Missing OpenID Provider URI");
    }

    @Nonnull
    @Override
    public RegistrationRequestBuilder atClientRegistrationEndpoint(@Nonnull final URI issuersClientRegistrationEndpointURI) {
      this.clientRegistrationEndpointURI = requireNonNull(issuersClientRegistrationEndpointURI, "Missing issuer's Client Registration Endpoint URI");
      return this;
    }

    @Nonnull
    @Override
    public RegistrationRequestBuilder withClientRegistrationEndpointFromIssuers(@Nonnull final OpenIDConfig configOfIssuer) throws MissingPropertyException {
      return atClientRegistrationEndpoint(requireNonNull(configOfIssuer, "Missing Federation Entity Config of issuer")
              .getFederationRegistrationEndpointURI());
    }

    @Nonnull
    @Override
    public RegistrationRequestBuilder atResolvedClientRegistrationEndpoint() throws HTTPException {
      return atClientRegistrationEndpoint(openIDConfigClient.openIDConfigOf(openIdProvider)
              .as(OpenIDConfig.class)
              .getFederationRegistrationEndpointURI());
    }

    @Nonnull
    @Override
    public JWSOf<ExplicitClientRegistrationResponse> explicitly(@Nonnull final JWSOfSelfSigned<ExplicitClientRegistrationRequest> request) throws HTTPException {
      final String clientRegistrationRequestBody = requireNonNull(request, "Missing Explicit Client Registration request to issue")
              .getJWS().serialize();
      try {
        final String responseBody = httpClient.sendingTo(clientRegistrationEndpointURI)
                .asMediaType(APPLICATION_JOSE_UTF8)
                .responseBodyAfterPOSTing(clientRegistrationRequestBody);
        final JWSOf<ExplicitClientRegistrationResponse> jwsOfClientRegistrationResponse = JWSOf.from(responseBody, ExplicitClientRegistrationResponse.class);
        LOG.info("Posted to OpenID Provider '{}' an Explicit Client Registration request {}. Response: {}", openIdProvider, request, jwsOfClientRegistrationResponse);
        return jwsOfClientRegistrationResponse;
      } catch (final HTTPResponseException e) {
        LOG.info("Posting an Explicit Client Registration request to OpenID Provider endpoint '{}' failed. Response code '{} {}', body: '{}'",
                clientRegistrationEndpointURI, e.getStatusCode(), e.getStatusReason(), shortenedBody(e.getResponseBody()));
        throw e;
      } catch (final HTTPException e) {
        LOG.info("Posting an Explicit Client Registration request to OpenID Provider endpoint '{}' failed: {} ({})",
                clientRegistrationEndpointURI, e.getMessage(), e.getClass().getName());
        throw e;
      } catch (final RuntimeException e) {
        LOG.warn("Posting an Explicit Client Registration request to OpenID Provider endpoint '{}' failed.",
                clientRegistrationEndpointURI, e);
        throw e;
      }
    }

  }

}
