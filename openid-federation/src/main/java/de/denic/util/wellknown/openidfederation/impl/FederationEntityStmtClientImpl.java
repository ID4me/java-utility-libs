/*^
  ===========================================================================
  ID4me Utility Libs
  ===========================================================================
  Copyright (C) 2020 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.util.wellknown.openidfederation.impl;

import de.denic.util.http.HTTPClient;
import de.denic.util.http.HTTPException;
import de.denic.util.wellknown.MissingPropertyException;
import de.denic.util.wellknown.openidfederation.FederationEntityConfig;
import de.denic.util.wellknown.openidfederation.FederationEntityConfigClient;
import de.denic.util.wellknown.openidfederation.FederationEntityStmtClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.annotation.concurrent.Immutable;
import javax.annotation.concurrent.NotThreadSafe;
import java.net.URI;
import java.util.Map;
import java.util.Optional;

import static java.util.Objects.requireNonNull;
import static java.util.Optional.empty;
import static java.util.Optional.of;

@Immutable
public final class FederationEntityStmtClientImpl implements FederationEntityStmtClient {

  private static final Logger LOG = LoggerFactory.getLogger(FederationEntityStmtClientImpl.class);

  private final HTTPClient httpClient;
  private final FederationEntityConfigClient federationEntityConfigClient;

  public FederationEntityStmtClientImpl(@Nonnull final HTTPClient httpClient,
                                        @Nonnull final FederationEntityConfigClient federationEntityConfigClient) {
    this.httpClient = requireNonNull(httpClient, "Missing HTTP client");
    this.federationEntityConfigClient = requireNonNull(federationEntityConfigClient, "Missing Federation Entity Configuration Client");
  }

  public FederationEntityStmtClientImpl(@Nonnull final HTTPClient httpClient) {
    this(httpClient, new FederationEntityConfigClientImpl(httpClient));
  }

  @Nonnull
  @Override
  public FederationAPIEndpointRequester federationEntityStmtIssuedBy(@Nonnull final URI issuer) {
    return new MyHandler(issuer);
  }

  @NotThreadSafe
  private class MyHandler implements FederationAPIEndpointRequester, FederationEntityStmtSubjectRequester {

    private final URI issuer;
    private URI issuersFederationAPIEndpointURI;
    private FederationEntityConfig optFederationEntityConfigOfIssuer;

    MyHandler(@Nonnull final URI issuer) {
      this.issuer = requireNonNull(issuer, "Missing issuer");
    }

    @Nonnull
    @Override
    public FederationEntityStmtSubjectRequester atIssuersEndpoint(@Nonnull final URI issuersFederationAPIEndpointURI) {
      this.issuersFederationAPIEndpointURI = requireNonNull(issuersFederationAPIEndpointURI, "Missing Federation API Endpoint URI");
      return this;
    }

    @Nonnull
    @Override
    public FederationEntityStmtSubjectRequester withEndpointFromIssuers(@Nonnull final FederationEntityConfig configOfIssuer) throws MissingPropertyException {
      this.optFederationEntityConfigOfIssuer = requireNonNull(configOfIssuer, "Missing Federation Entity Configuration");
      final URI federationApiEndpointURI = configOfIssuer.getFederationApiEndpoint();
      return atIssuersEndpoint(federationApiEndpointURI);
    }

    @Nonnull
    @Override
    public FederationEntityStmtSubjectRequester atIssuersResolvedEndpoint() throws HTTPException {
      this.optFederationEntityConfigOfIssuer = federationEntityConfigClient.federationEntityConfigOf(issuer)
              .asJWSOfSelfSigned(FederationEntityConfig.class)
              .getPayload();
      final URI federationAPIEndpoint = this.optFederationEntityConfigOfIssuer
              .getFederationApiEndpoint();
      return atIssuersEndpoint(federationAPIEndpoint);
    }

    @Nonnull
    @Override
    public Optional<FederationEntityStmtSubjectRequester> optAtIssuersResolvedEndpoint() throws HTTPException {
      try {
        return of(atIssuersResolvedEndpoint());
      } catch (final HTTPException e) {
        LOG.warn("Querying Federation API Endpoint of '{}' failed.", issuer, e);
        return empty();
      }
    }

    @Nonnull
    @Override
    public ResponseConverter regarding(@Nonnull final URI subject) throws HTTPException {
      final Map<String, Object> queryParameters = Map.of("iss", issuer, "sub", requireNonNull(subject, "Missing subject"));
      return new FederationEntityStmtResponseConverter(issuer,
              optFederationEntityConfigOfIssuer,
              httpClient.responseBodyQuerying(issuersFederationAPIEndpointURI, queryParameters),
              subject);
    }

    @Nonnull
    @Override
    public Optional<ResponseConverter> optRegarding(@Nonnull final URI subject) throws HTTPException {
      try {
        return of(regarding(subject));
      } catch (final HTTPException e) {
        LOG.warn("Querying Federation Entity Statement of issuer '{}' regarding subject '{}' failed.", issuer, subject, e);
        return empty();
      }
    }

  }

}
