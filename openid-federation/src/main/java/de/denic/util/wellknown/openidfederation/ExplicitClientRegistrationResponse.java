/*^
  ===========================================================================
  ID4me Utility Libs
  ===========================================================================
  Copyright (C) 2020 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.util.wellknown.openidfederation;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.nimbusds.jose.jwk.JWKSet;
import de.denic.util.wellknown.openidfederation.impl.CommonFederationEntityStmt;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;
import java.io.IOException;
import java.net.URI;
import java.util.Collection;
import java.util.Objects;
import java.util.Set;

import static de.denic.util.wellknown.openidfederation.impl.RequirementUtil.requireEqual;
import static java.util.Collections.emptySet;
import static java.util.Objects.requireNonNull;
import static java.util.Objects.requireNonNullElse;
import static java.util.stream.Collectors.toUnmodifiableSet;

@JsonDeserialize(as = ExplicitClientRegistrationResponse.Impl.class)
public interface ExplicitClientRegistrationResponse extends CommonFederationEntityStmt {

  /**
   * @return Usually immutable!
   */
  @Nonnull
  Set<URI> getAuthorityHints();

  @Nonnull
  RelyingPartyMetaDataPolicy getRelyingPartyMetaDataPolicy();

  @Immutable
  @JsonIgnoreProperties(ignoreUnknown = true)
  final class Impl extends CommonFederationEntityStmt.Impl implements ExplicitClientRegistrationResponse {

    private static final Collection<URI> NO_AUTHORITY_HINTS = emptySet();
    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper().registerModule(new Jdk8Module());
    private static final ObjectReader RELYING_PARTY_META_DATA_POLICY_READER = OBJECT_MAPPER.readerFor(RelyingPartyMetaDataPolicy.class);
    private static final JWKSet NO_JWK_SET_OF_SUBJECT = null;

    private final Set<URI> fixedAuthorityHints;
    private final RelyingPartyMetaDataPolicy relyingPartyMetaDataPolicy;

    /**
     * @see CommonFederationEntityStmt.Impl#Impl(URI, URI, Long, Long, JWKSet)
     */
    public Impl(@Nonnull final URI issuer,
                @Nonnull final URI subject,
                @Nonnull final Long issuedAt,
                @Nonnull final Long expiresAt,
                @Nonnull final RelyingPartyMetaDataPolicy relyingPartyMetaDataPolicy,
                @Nullable final Collection<URI> authorityHints) {
      super(issuer, subject, issuedAt, expiresAt, NO_JWK_SET_OF_SUBJECT);
      this.relyingPartyMetaDataPolicy = requireNonNull(relyingPartyMetaDataPolicy, "Missing relying party meta-data policy (JSON key 'metadata_policy')");
      this.fixedAuthorityHints = requireNonNullElse(authorityHints, NO_AUTHORITY_HINTS).stream()
              .filter(Objects::nonNull)
              .collect(toUnmodifiableSet());
    }

    public Impl(@Nonnull @JsonProperty("iss") final URI issuer,
                @Nonnull @JsonProperty("sub") final URI subject,
                @Nonnull @JsonProperty("aud") final URI audience,
                @Nonnull @JsonProperty("iat") final Long issuedAt,
                @Nonnull @JsonProperty("exp") final Long expiresAt,
                @Nonnull @JsonProperty("metadata_policy") final JsonNode metaDataPolicyJsonNode,
                @Nullable @JsonProperty("authority_hints") final Collection<URI> authorityHints) {
      this(issuer, requireEqual(subject, audience),
              issuedAt, expiresAt,
              extractRelyingPartyMetaDataPolicyFrom(requireNonNull(metaDataPolicyJsonNode, "Missing relying party meta-data policy (JSON key 'metadata_policy')")),
              authorityHints);
    }

    @Nonnull
    private static RelyingPartyMetaDataPolicy extractRelyingPartyMetaDataPolicyFrom(@Nonnull final JsonNode metaDataPolicyJsonNode) {
      final JsonNode openIDRelyingPartyNode = metaDataPolicyJsonNode.path("openid_relying_party");
      try {
        return RELYING_PARTY_META_DATA_POLICY_READER.readValue(openIDRelyingPartyNode);
      } catch (final IOException e) {
        throw new IllegalArgumentException("Parsing meta-data policy JSON node failed", e);
      }
    }

    @Override
    @Nonnull
    public RelyingPartyMetaDataPolicy getRelyingPartyMetaDataPolicy() {
      return relyingPartyMetaDataPolicy;
    }

    @Nonnull
    @Override
    public Set<URI> getAuthorityHints() {
      return fixedAuthorityHints;
    }

    @Override
    public boolean equals(final Object o) {
      if (this == o)
        return true;
      if (o == null || getClass() != o.getClass())
        return false;
      if (!super.equals(o))
        return false;

      final ExplicitClientRegistrationResponse.Impl impl = (ExplicitClientRegistrationResponse.Impl) o;

      if (!fixedAuthorityHints.equals(impl.fixedAuthorityHints))
        return false;
      return relyingPartyMetaDataPolicy.equals(impl.relyingPartyMetaDataPolicy);
    }

    @Override
    public int hashCode() {
      int result = super.hashCode();
      result = 31 * result + fixedAuthorityHints.hashCode();
      result = 31 * result + relyingPartyMetaDataPolicy.hashCode();
      return result;
    }

    @Override
    public String toString() {
      return "{" + super.toString() +
              ", relyingPartyMetaData=" + relyingPartyMetaDataPolicy +
              ", authorityHints=" + fixedAuthorityHints +
              '}';
    }

  }

}
