/*^
  ===========================================================================
  ID4me Utility Libs
  ===========================================================================
  Copyright (C) 2020 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.util.wellknown.openidfederation.impl;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.JsonNode;
import com.nimbusds.jose.jwk.JWKSet;
import de.denic.util.jws.JWKSetSupplier;
import de.denic.util.jws.JWSOfSelfSigned;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;
import java.net.URI;
import java.text.ParseException;
import java.time.ZonedDateTime;
import java.util.Map;

import static de.denic.util.wellknown.openidfederation.impl.ValidityConstrained.fromUTCEpochSeconds;
import static de.denic.util.wellknown.openidfederation.impl.ValidityConstrained.optFromUTCEpochSeconds;
import static java.util.Objects.requireNonNull;

public interface CommonFederationEntityStmt extends JWKSetSupplier, ValidityConstrained {

  @JsonGetter("iss")
  @Nonnull
  URI getIssuer();

  @JsonGetter("sub")
  @Nonnull
  URI getSubject();

  /**
   * Same as {@link #getJWKSet()}, but semantically more specific.
   *
   * @see #getJWKSet()
   */
  @JsonIgnore
  @Nonnull
  default JWKSet getJWKSetOfSubject() {
    return getJWKSet();
  }

  @JsonIgnore
  @Nonnull
  default JWKSet getPublicJWKSetOfSubject() {
    return getJWKSet().toPublicJWKSet();
  }

  @JsonIgnore
  default boolean isSelfSigned() {
    return getIssuer().equals(getSubject());
  }

  @Nonnull
  <PTYPE extends JWKSetSupplier> JWSOfSelfSigned<PTYPE> sign(@Nonnull PTYPE toSign);

  @Immutable
  abstract class Impl extends ValidityConstrained.Impl implements CommonFederationEntityStmt {

    private final URI issuer, subject;
    private final JWKSet jwkSetOfSubject;

    /**
     * @param expiresAt <code>null</code> if <strong>NOT</strong> expiring!
     */
    protected Impl(@Nonnull final URI issuer,
                   @Nonnull final URI subject,
                   @Nonnull final ZonedDateTime issuedAt,
                   @Nullable final ZonedDateTime expiresAt,
                   @Nullable final JWKSet jwkSetOfSubject) {
      super(issuedAt, expiresAt);
      this.issuer = requireNonNull(issuer, "Missing issuer (JSON key 'iss')");
      this.subject = requireNonNull(subject, "Missing subject (JSON key 'sub')");
      this.jwkSetOfSubject = (jwkSetOfSubject == null ? new JWKSet() : jwkSetOfSubject);
    }

    /**
     * @param expiresAt <code>0L</code> if <strong>NOT</strong> expiring!
     */
    protected Impl(@Nonnull final URI issuer,
                   @Nonnull final URI subject,
                   @Nonnull final Long issuedAt,
                   @Nonnull final Long expiresAt,
                   @Nullable final JWKSet jwkSetOfSubject) {
      this(issuer, subject,
              fromUTCEpochSeconds(issuedAt, "Missing issuing timestamp (JSON key 'iat')"),
              optFromUTCEpochSeconds(expiresAt, "Missing expiration timestamp (JSON key 'exp')"),
              jwkSetOfSubject);
    }

    @Nonnull
    protected static JWKSet parseToJWKSet(@Nonnull final JsonNode jsonNode) {
      if (requireNonNull(jsonNode, "Missing JWK-set (JSON key 'jwks')").isNull()) {
        throw new IllegalArgumentException("Missing JWK-set (JSON key 'jwks')");
      }

      try {
        return JWKSet.parse(jsonNode.toString());
      } catch (final ParseException e) {
        throw new IllegalArgumentException("Parsing JSON value of JWK-set (JSON key 'jwks') failed", e);
      }
    }

    @Nonnull
    @Override
    public final URI getIssuer() {
      return issuer;
    }

    @Nonnull
    @Override
    public final URI getSubject() {
      return subject;
    }

    @Nonnull
    @Override
    public JWKSet getJWKSet() {
      return jwkSetOfSubject;
    }

    @JsonGetter("jwks")
    @Nullable
    public final Map<String, Object> getPublicJWKSetOfSubjectAsJSON() {
      final JWKSet publicJWKSet = getPublicJWKSetOfSubject();
      return (publicJWKSet.getKeys().isEmpty() ? null : publicJWKSet.toJSONObject());
    }

    @Nonnull
    @Override
    public <PTYPE extends JWKSetSupplier> JWSOfSelfSigned<PTYPE> sign(@Nonnull final PTYPE toSign) {
      return JWSOfSelfSigned.signedWithIts1stJWK(toSign);
    }

    @Override
    public boolean equals(final Object o) {
      if (this == o)
        return true;
      if (o == null || getClass() != o.getClass())
        return false;
      if (!super.equals(o))
        return false;

      final CommonFederationEntityStmt.Impl impl = (CommonFederationEntityStmt.Impl) o;

      if (!issuer.equals(impl.issuer))
        return false;
      if (!subject.equals(impl.subject))
        return false;
      // Hint: Seems type JWKSet itself does NOT implement equals() correctly:
      return jwkSetOfSubject.getKeys().equals(impl.jwkSetOfSubject.getKeys());
    }

    @Override
    public int hashCode() {
      int result = super.hashCode();
      result = 31 * result + issuer.hashCode();
      result = 31 * result + subject.hashCode();
      result = 31 * result + jwkSetOfSubject.hashCode();
      return result;
    }

    @Override
    public String toString() {
      return "issuer='" + issuer +
              "', subject='" + subject +
              "', " + super.toString() +
              ", jwkSetOfSubject=" + jwkSetOfSubject;
    }

  }

}
