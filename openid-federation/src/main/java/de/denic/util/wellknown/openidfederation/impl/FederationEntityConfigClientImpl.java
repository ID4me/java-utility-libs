/*^
  ===========================================================================
  ID4me Utility Libs
  ===========================================================================
  Copyright (C) 2020 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.util.wellknown.openidfederation.impl;

import de.denic.util.http.HTTPClient;
import de.denic.util.http.HTTPException;
import de.denic.util.http.HTTPResponseException;
import de.denic.util.wellknown.openidfederation.FederationEntityConfigClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.annotation.concurrent.Immutable;
import java.net.URI;
import java.util.Optional;

import static de.denic.util.http.HTTPResponseException.NOT_FOUND;
import static java.util.Objects.requireNonNull;
import static java.util.Optional.empty;
import static java.util.Optional.of;

@Immutable
public final class FederationEntityConfigClientImpl implements FederationEntityConfigClient {

  private static final Logger LOG = LoggerFactory.getLogger(FederationEntityConfigClientImpl.class);
  private static final String WELL_KNOWN_OPENID_FEDERATION = "/.well-known/openid-federation";

  private final HTTPClient httpClient;

  public FederationEntityConfigClientImpl(@Nonnull final HTTPClient httpClient) {
    this.httpClient = requireNonNull(httpClient, "Missing HTTP client");
  }

  @Nonnull
  @Override
  public ResponseConverter federationEntityConfigOf(@Nonnull final URI target) throws HTTPException {
    final URI rfc8615CompliantWellKnownURI = httpClient.uriBuilderOf(target)
            .replacePath(WELL_KNOWN_OPENID_FEDERATION)
            .addPath(target.getPath())
            .build();
    try {
      return new FederationEntityConfigResponseConverter(httpClient.responseBodyQuerying(rfc8615CompliantWellKnownURI), target);
    } catch (final HTTPResponseException e) {
      if (e.getStatusCode() == NOT_FOUND && !(target.getPath().isEmpty())) {
        return fallbackToRFC8615IncompliantWellKnownURI(target);
      }

      throw e;
    }
  }

  /**
   * Implements fallback to RFC8615-incompliant Well-Known target URI as recommended by
   * <a href="https://openid.net/specs/openid-connect-federation-1_0.html#federation_configuration">OpenID Connect Federation 1.0 - draft 12, chapter 'Obtaining Federation Entity Configuration Information'</a>
   */
  @Nonnull
  private ResponseConverter fallbackToRFC8615IncompliantWellKnownURI(@Nonnull final URI target) {
    final URI recommendedRFC8615IncompliantWellKnownURI = httpClient.uriBuilderOf(target)
            .addPath(WELL_KNOWN_OPENID_FEDERATION)
            .build();
    LOG.debug("Querying for Federation Entity Configuration of '{}': Fallback to NON RFC8615-compliant URL '{}'", target, recommendedRFC8615IncompliantWellKnownURI);
    return new FederationEntityConfigResponseConverter(httpClient.responseBodyQuerying(recommendedRFC8615IncompliantWellKnownURI), target);
  }

  @Override
  public Optional<ResponseConverter> optFederationEntityConfigOf(@Nonnull final URI target) {
    try {
      return of(federationEntityConfigOf(target));
    } catch (final HTTPException e) {
      LOG.warn("Querying Federation Entity Configuration of '{}' failed.", target, e);
      return empty();
    }
  }

}
