/*^
  ===========================================================================
  ID4me Utility Libs
  ===========================================================================
  Copyright (C) 2020 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.util.wellknown.openidfederation;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;
import java.net.URI;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;

import static java.util.Collections.emptySet;
import static java.util.Locale.ENGLISH;
import static java.util.Objects.requireNonNull;
import static java.util.Objects.requireNonNullElse;
import static java.util.Optional.ofNullable;
import static java.util.stream.Collectors.toUnmodifiableSet;

@JsonSerialize(as = RelyingPartyMetaData.Impl.class)
@JsonDeserialize(as = RelyingPartyMetaData.Impl.class)
public interface RelyingPartyMetaData {

  /**
   * @return Usually unmodifiable.
   */
  @JsonGetter("redirect_uris")
  @Nonnull
  Set<URI> getRedirectURIs();

  @JsonGetter("client_name")
  @Nonnull
  String getClientName();

  @JsonGetter("logo_uri")
  @Nonnull
  Optional<URI> getLogoURI();

  /**
   * @return Usually unmodifiable.
   */
  @JsonGetter("client_registration_types")
  @Nonnull
  Set<String> getClientRegistrationTypes();

  @Immutable
  @JsonIgnoreProperties(ignoreUnknown = true)
  final class Impl implements RelyingPartyMetaData {

    public static final Set<String> NO_CLIENT_REGISTRATION_TYPES = emptySet();
    public static final Set<String> EXPLICIT_ONLY_CLIENT_REGISTRATION_TYPES = Set.of("explicit");

    private final Set<URI> fixedRedirectURIs;
    private final String clientName;
    private final URI logoURI;
    private final Set<String> fixedClientRegistrationTypes;

    public Impl(@JsonProperty("client_name") @Nonnull final String clientName,
                @JsonProperty("redirect_uris") @Nonnull final Collection<URI> redirectURIs,
                @JsonProperty("logo_uri") @Nullable final URI logoURI,
                @JsonProperty("client_registration_types") @Nullable final Collection<String> clientRegistrationTypes) {
      this.fixedRedirectURIs = requireNonNull(redirectURIs, "Missing redirect URIs (JSON key 'redirect_uris')").stream()
              .filter(Objects::nonNull)
              .collect(toUnmodifiableSet());
      if (this.fixedRedirectURIs.isEmpty()) {
        throw new IllegalArgumentException("Missing redirect URIs (JSON key 'redirect_uris')");
      }

      this.clientName = requireNonNull(clientName, "Missing client name (JSON key 'client_name')");
      this.logoURI = logoURI;
      this.fixedClientRegistrationTypes = requireNonNullElse(clientRegistrationTypes, NO_CLIENT_REGISTRATION_TYPES).stream()
              .filter(Objects::nonNull)
              .map(type -> type.toLowerCase(ENGLISH))
              .collect(toUnmodifiableSet());
    }

    /**
     * Applies {@link #EXPLICIT_ONLY_CLIENT_REGISTRATION_TYPES} as Client Registration Types.
     */
    public Impl(@Nonnull final String clientName,
                @Nonnull final Collection<URI> redirectURIs,
                @Nullable final URI logoURI) {
      this(clientName, redirectURIs, logoURI, EXPLICIT_ONLY_CLIENT_REGISTRATION_TYPES);
    }

    @Nonnull
    @Override
    public Set<URI> getRedirectURIs() {
      return fixedRedirectURIs;
    }

    @Nonnull
    @Override
    public String getClientName() {
      return clientName;
    }

    @Nonnull
    @Override
    public Optional<URI> getLogoURI() {
      return ofNullable(logoURI);
    }

    @Nonnull
    @Override
    public Set<String> getClientRegistrationTypes() {
      return fixedClientRegistrationTypes;
    }

    @Override
    public boolean equals(final Object o) {
      if (this == o)
        return true;
      if (o == null || getClass() != o.getClass())
        return false;

      final Impl impl = (Impl) o;

      if (!fixedRedirectURIs.equals(impl.fixedRedirectURIs))
        return false;
      if (!clientName.equals(impl.clientName))
        return false;
      if (!fixedClientRegistrationTypes.equals(impl.fixedClientRegistrationTypes))
        return false;
      return logoURI != null ? logoURI.equals(impl.logoURI) : impl.logoURI == null;
    }

    @Override
    public int hashCode() {
      int result = fixedRedirectURIs.hashCode();
      result = 31 * result + clientName.hashCode();
      result = 31 * result + (logoURI != null ? logoURI.hashCode() : 0);
      result = 31 * result + fixedClientRegistrationTypes.hashCode();
      return result;
    }

    @Override
    public String toString() {
      return "{redirectURIs=" + fixedRedirectURIs +
              ", clientName='" + clientName + '\'' +
              ", logoURI=" + logoURI +
              ", clientRegTypes=" + fixedClientRegistrationTypes +
              '}';
    }

  }

}
