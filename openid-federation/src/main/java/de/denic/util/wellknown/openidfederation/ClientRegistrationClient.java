/*^
  ===========================================================================
  ID4me Utility Libs
  ===========================================================================
  Copyright (C) 2020 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.util.wellknown.openidfederation;

import de.denic.util.http.HTTPException;
import de.denic.util.jws.JWSOf;
import de.denic.util.jws.JWSOfSelfSigned;
import de.denic.util.wellknown.MissingPropertyException;
import de.denic.util.wellknown.openidconfig.OpenIDConfig;

import javax.annotation.Nonnull;
import java.net.URI;

/**
 * Implements <a href="https://openid.net/specs/openid-connect-federation-1_0.html#rfc.section.9.2">OpenID Connect Federation Explicit Client Registration</a>
 */
public interface ClientRegistrationClient {

  String APPLICATION_JOSE_UTF8 = "application/jose;charset=UTF-8";

  @Nonnull
  ClientRegistrationEndpointRequester registerClientWith(@Nonnull URI openIdProvider);

  interface ClientRegistrationEndpointRequester {

    @Nonnull
    RegistrationRequestBuilder atClientRegistrationEndpoint(@Nonnull URI issuersClientRegistrationEndpointURI);

    @Nonnull
    RegistrationRequestBuilder withClientRegistrationEndpointFromIssuers(@Nonnull OpenIDConfig configOfIssuer) throws MissingPropertyException;

    @Nonnull
    RegistrationRequestBuilder atResolvedClientRegistrationEndpoint() throws HTTPException;

  }

  interface RegistrationRequestBuilder {

    @Nonnull
    JWSOf<ExplicitClientRegistrationResponse> explicitly(@Nonnull JWSOfSelfSigned<ExplicitClientRegistrationRequest> request) throws HTTPException;

  }

}
