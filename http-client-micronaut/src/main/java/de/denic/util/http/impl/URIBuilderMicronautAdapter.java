/*^
  ===========================================================================
  ID4me Utility Libs
  ===========================================================================
  Copyright (C) 2020 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.util.http.impl;

import de.denic.util.http.URIBuilder;
import io.micronaut.http.uri.UriBuilder;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.NotThreadSafe;
import java.net.URI;

@NotThreadSafe
public final class URIBuilderMicronautAdapter implements URIBuilder {

  private final UriBuilder uriBuilder;

  public URIBuilderMicronautAdapter(@Nonnull final URI initialURI) {
    this.uriBuilder = UriBuilder.of(initialURI);
  }

  @Nonnull
  @Override
  public URIBuilder replacePath(@Nullable final String newPath) {
    uriBuilder.replacePath(newPath);
    return this;
  }

  @Nonnull
  @Override
  public URIBuilder addPath(@Nonnull final String path) {
    uriBuilder.path(path);
    return this;
  }

  @Nonnull
  @Override
  public URIBuilder queryParam(@Nonnull final String key, @Nonnull final Object param) {
    uriBuilder.queryParam(key, param);
    return this;
  }

  @Nonnull
  @Override
  public URI build() {
    return uriBuilder.build();
  }

}
