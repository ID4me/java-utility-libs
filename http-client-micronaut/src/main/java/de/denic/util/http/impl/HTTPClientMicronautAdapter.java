/*^
  ===========================================================================
  ID4me Utility Libs
  ===========================================================================
  Copyright (C) 2020 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.util.http.impl;

import de.denic.util.http.*;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.HttpStatus;
import io.micronaut.http.MediaType;
import io.micronaut.http.MutableHttpRequest;
import io.micronaut.http.client.HttpClient;
import io.micronaut.http.client.exceptions.HttpClientException;
import io.micronaut.http.client.exceptions.HttpClientResponseException;
import io.micronaut.http.uri.UriBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;
import javax.annotation.concurrent.NotThreadSafe;
import java.net.URI;
import java.util.Map;

import static de.denic.util.http.HTTPClient.replaceVerticalWhiteSpaces;
import static de.denic.util.http.HTTPClient.shortenedBody;
import static io.micronaut.http.HttpRequest.GET;
import static io.micronaut.http.HttpRequest.POST;
import static java.util.Objects.requireNonNull;

@Immutable
public final class HTTPClientMicronautAdapter implements HTTPClient {

  private static final Logger LOG = LoggerFactory.getLogger(HTTPClientMicronautAdapter.class);

  private final HttpClient httpClient;

  public HTTPClientMicronautAdapter(@Nonnull final HttpClient httpClient) {
    this.httpClient = requireNonNull(httpClient, "Missing HTTP client");
  }

  @Nonnull
  @Override
  public String responseBodyQuerying(@Nonnull final URI wellKnownURI,
                                     @Nullable final Map<String, Object> optQueryParameters) throws HTTPException {
    final URI targetURI;
    if (optQueryParameters == null || optQueryParameters.isEmpty()) {
      targetURI = wellKnownURI;
    } else {
      final UriBuilder targetURIBuilder = UriBuilder.of(requireNonNull(wellKnownURI, "Missing well-known URI"));
      optQueryParameters.forEach(targetURIBuilder::queryParam);
      targetURI = targetURIBuilder.build();
    }
    return executeBlocking("Queried", GET(targetURI), "Querying");
  }

  @Nonnull
  @Override
  public RequestParamSetter sendingTo(@Nonnull final URI targetURI) {
    return new RequestBuilderImpl(targetURI);
  }

  @Nonnull
  private String executeBlocking(@Nonnull final String successLogMessagePrefix,
                                 @Nonnull final MutableHttpRequest<?> request,
                                 @Nonnull final String failureLogMessagePrefix) {
    try {
      final String responseBody = httpClient.toBlocking().retrieve(request);
      if (LOG.isDebugEnabled()) {
        LOG.debug("{} '{}': '{}'", successLogMessagePrefix, request.getUri(), replaceVerticalWhiteSpaces(responseBody));
      }
      return responseBody;
    } catch (final HttpClientResponseException e) {
      final HttpStatus httpStatus = e.getStatus();
      final String responseBody = bodyOf(e.getResponse());
      if (LOG.isDebugEnabled()) {
        LOG.debug("{} '{}' failed. Response code '{} {}', body: '{}'",
                failureLogMessagePrefix, request.getUri(), httpStatus.getCode(), httpStatus.getReason(), shortenedBody(responseBody));
      }
      throw new HTTPResponseException(httpStatus.getCode(), httpStatus.getReason(), responseBody);
    } catch (final HttpClientException e) {
      LOG.info("{} '{}' failed: {} ({})", failureLogMessagePrefix, request.getUri(), e.getMessage(), e.getClass().getName());
      throw new HTTPException(e.getMessage());
    } catch (final RuntimeException e) {
      LOG.warn("{} '{}' failed.", failureLogMessagePrefix, request.getUri(), e);
      throw e;
    }
  }

  @Nonnull
  @Override
  public URIBuilder uriBuilderOf(@Nonnull final URI initialURI) {
    return new URIBuilderMicronautAdapter(initialURI);
  }

  @Nullable
  private static String bodyOf(@Nullable final HttpResponse<?> httpResponse) {
    if (httpResponse == null) {
      return null;
    }

    return httpResponse.getBody(String.class).orElse(null);
  }

  @NotThreadSafe
  private class RequestBuilderImpl implements RequestParamSetter, RequestParamSetter.RequestSender {

    private final URI targetURI;
    private MediaType mediaType;

    RequestBuilderImpl(final URI targetURI) {
      this.targetURI = requireNonNull(targetURI, "Missing target URI");
    }

    @Nonnull
    @Override
    public RequestParamSetter.RequestSender asMediaType(@Nonnull final String mediaType) {
      this.mediaType = MediaType.of(requireNonNull(mediaType, "Missing media type"));
      return this;
    }

    @Nonnull
    @Override
    public String responseBodyAfterPOSTing(@Nonnull final String requestBody) throws HTTPException {
      final MutableHttpRequest<String> request = POST(targetURI, requestBody)
              .contentType(mediaType);
      return executeBlocking("POSTed to", request, "POSTing to");
    }

  }

}
