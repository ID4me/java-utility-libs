/*^
  ===========================================================================
  ID4me Utility Libs
  ===========================================================================
  Copyright (C) 2020 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.util.http;

import javax.annotation.Nullable;
import javax.annotation.concurrent.NotThreadSafe;

import static de.denic.util.http.HTTPClient.shortenedBody;

@NotThreadSafe
public final class HTTPResponseException extends HTTPException {

  private static final long serialVersionUID = -1401965045286281515L;

  public static final int NOT_FOUND = 404;

  private final int statusCode;
  private final String statusReason;
  private final String responseBody;

  public HTTPResponseException(final String message, final int statusCode, final String statusReason, final String responseBody) {
    super((message == null ? "" : message + ". ") + "Status code '" + statusCode +
            (statusReason == null ? "" : " - " + statusReason) + "'. Body: " +
            shortenedBody(responseBody));
    this.statusCode = statusCode;
    this.statusReason = statusReason;
    this.responseBody = responseBody;
  }

  public HTTPResponseException(final String message, final int statusCode, final String statusReason) {
    this(message, statusCode, statusReason, null);
  }

  public HTTPResponseException(final int statusCode, final String statusReason, final String responseBody) {
    this(null, statusCode, statusReason, responseBody);
  }

  public int getStatusCode() {
    return statusCode;
  }

  @Nullable
  public String getStatusReason() {
    return statusReason;
  }

  @Nullable
  public String getResponseBody() {
    return responseBody;
  }

}
