/*^
  ===========================================================================
  ID4me Utility Libs
  ===========================================================================
  Copyright (C) 2020 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.util.http;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.annotation.concurrent.Immutable;
import javax.annotation.concurrent.NotThreadSafe;
import java.io.IOException;
import java.net.URI;
import java.util.Optional;
import java.util.function.BiFunction;
import java.util.function.Supplier;

import static java.util.Objects.requireNonNull;
import static java.util.Optional.empty;
import static java.util.Optional.of;

public interface HTTPResponseBodyToJSONConverter<EXCEPTION extends HTTPResponseBodyToJSONConverter.ConverterException> {

  @Nonnull
  JsonNode asJsonNode() throws EXCEPTION;

  @Nonnull
  Optional<JsonNode> asOptJsonNode();

  @Nonnull
  <RESULT> RESULT as(@Nonnull Class<? extends RESULT> resultType) throws EXCEPTION;

  @Nonnull
  <RESULT> Optional<RESULT> asOpt(@Nonnull Class<? extends RESULT> resultType);

  @Immutable
  abstract class Base<EX extends ConverterException> implements HTTPResponseBodyToJSONConverter<EX> {

    private static final Logger LOG = LoggerFactory.getLogger(Base.class);
    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();
    private static final ObjectReader READER = OBJECT_MAPPER.reader();

    private final String plainResponse;
    private final URI targetURI;
    private final String operationDescriptionForLogging;
    private final BiFunction<Exception, Class<?>, EX> parsingExceptionProvider;

    protected Base(@Nonnull final String plainResponse, @Nonnull final URI targetURI,
                   @Nonnull final String operationDescriptionForLogging,
                   @Nonnull final BiFunction<Exception, Class<?>, EX> parsingExceptionProvider) {
      this.plainResponse = requireNonNull(plainResponse, "Missing response");
      this.targetURI = requireNonNull(targetURI, "Missing target URI");
      this.operationDescriptionForLogging = requireNonNull(operationDescriptionForLogging, "Missing operation description");
      this.parsingExceptionProvider = requireNonNull(parsingExceptionProvider, "Missing exception provider");
    }

    @Nonnull
    @Override
    public final JsonNode asJsonNode() throws EX {
      try {
        final JsonNode jsonNode = READER.readTree(plainResponse);
        LOG.info("{} of '{}': {}", operationDescriptionForLogging, targetURI, jsonNode);
        return jsonNode;
      } catch (final JsonProcessingException e) {
        throw parsingExceptionProvider.apply(e, JsonNode.class);
      }
    }

    @Nonnull
    @Override
    public final Optional<JsonNode> asOptJsonNode() {
      return asOptional(this::asJsonNode);
    }

    @Nonnull
    @Override
    public final <RESULT> RESULT as(@Nonnull final Class<? extends RESULT> resultType) throws EX {
      final JsonNode jsonNode = asJsonNode();
      try {
        return READER.readValue(jsonNode, requireNonNull(resultType, "Missing result type"));
      } catch (final IOException e) {
        throw parsingExceptionProvider.apply(e, resultType);
      }
    }

    @Nonnull
    @Override
    public final <RESULT> Optional<RESULT> asOpt(@Nonnull final Class<? extends RESULT> resultType) {
      return asOptional(() -> as(resultType));
    }

    @Nonnull
    protected final <RESULT> Optional<RESULT> asOptional(@Nonnull final Supplier<? extends RESULT> supplier) {
      try {
        return of(requireNonNull(supplier, "Missing supplier").get());
      } catch (final ConverterException e) {
        LOG.warn(e.getMessage(), e);
        return empty();
      }
    }

    @Override
    public String toString() {
      return plainResponse;
    }

  }

  @NotThreadSafe
  abstract class ConverterException extends RuntimeException {

    private static final long serialVersionUID = 4405562502104210086L;

    protected ConverterException(final String message, final Throwable cause) {
      super(message, cause);
    }

  }

}