/*^
  ===========================================================================
  ID4me Utility Libs
  ===========================================================================
  Copyright (C) 2020 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.util.http;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.net.URI;
import java.util.Map;

import static java.util.Objects.requireNonNullElse;

public interface HTTPClient {

  Map<String, Object> NO_QUERY_PARAMETERS = null;
  String EMPTY_BODY = ">EMPTY<";

  @Nonnull
  default String responseBodyQuerying(@Nonnull URI wellKnownURI) throws HTTPException {
    return responseBodyQuerying(wellKnownURI, NO_QUERY_PARAMETERS);
  }

  @Nonnull
  String responseBodyQuerying(@Nonnull URI wellKnownURI,
                              @Nullable Map<String, Object> queryParameters) throws HTTPException;

  @Nonnull
  RequestParamSetter sendingTo(@Nonnull URI targetURI);

  @Nonnull
  URIBuilder uriBuilderOf(@Nonnull URI initialURI);

  @Nonnull
  static String shortenedBody(@Nullable final String responseBody) {
    String strippedBody = requireNonNullElse(responseBody, EMPTY_BODY).strip();
    if (strippedBody.length() > 256) {
      strippedBody = strippedBody.substring(0, 256) + "[...SHORTENED...]";
    }
    return replaceVerticalWhiteSpaces(strippedBody);
  }

  @Nonnull
  static String replaceVerticalWhiteSpaces(@Nullable final String input) {
    if (input == null) {
      return ">NULL<";
    }

    return input.replaceAll("\\v+", "|>>");
  }

}
