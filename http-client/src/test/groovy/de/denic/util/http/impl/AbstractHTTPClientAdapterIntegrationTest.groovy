/*^
  ===========================================================================
  ID4me Utility Libs
  ===========================================================================
  Copyright (C) 2020 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.util.http.impl

import de.denic.util.http.HTTPClient
import de.denic.util.http.HTTPResponseException
import spock.lang.Specification

abstract class AbstractHTTPClientAdapterIntegrationTest extends Specification {

  HTTPClient CUT

  def setup() {
    CUT = createCUT()
  }

  abstract HTTPClient createCUT()

  final def 'Querying for response body delivers it'() {
    given:
    def existingResource = URI.create('https://accounts.google.com/.well-known/openid-configuration')

    expect:
    CUT.responseBodyQuerying(existingResource)
            .contains('"issuer": "https://accounts.google.com"')
  }

  final def 'Querying for response body of unknown resource throws appropriate exception'() {
    given:
    def notExistingResource = URI.create('http://example.com/.well-known/openid-configuration')

    when:
    CUT.responseBodyQuerying(notExistingResource)

    then:
    def exception = thrown(HTTPResponseException)
    exception.statusCode == 404
    println "Caught: ${exception}"
  }

  final def 'POSTing to existing resource not handling such requests'() {
    given:
    def existingResource = URI.create('https://id.denic.de/.well-known/openid-configuration')

    when:
    CUT.sendingTo(existingResource)
            .asMediaType('application/xml')
            .responseBodyAfterPOSTing('FOO')

    then:
    def exception = thrown(HTTPResponseException)
    exception.statusCode == 405
    println "Caught after POSTing to existing resource: ${exception}"
  }

  final def 'POSTing to non-existing resource'() {
    given:
    def notExistingResource = URI.create('http://example.com/.well-known/openid-configuration')

    when:
    CUT.sendingTo(notExistingResource)
            .asMediaType('application/xml')
            .responseBodyAfterPOSTing('FOO')

    then:
    def exception = thrown(HTTPResponseException)
    exception.statusCode == 404
    println "Caught after POSTing to non-existing resource: ${exception}"
  }

}
